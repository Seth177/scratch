const secondsToTime = require('./secondsToTime.js'),
    gettime = require('./gettime.js'),
    isInArray = require('./isInArray.js'),
    toSentenceCase = require('./toSentenceCase.js');
exports.creationDate = require('./creationDate.js');

const util = {
    secondsToTime,
    gettime,
    isInArray,
    toSentenceCase
};
exports.util = util;