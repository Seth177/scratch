/*/Quick way of checking if something is in a array/*/
module.exports = function(value, array) {
	return array.indexOf(value) > -1;
};