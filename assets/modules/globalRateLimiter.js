/*
    Rate Limiter

    Author:  Frosthaven
    Twitter: @thefrosthaven

    Description:
        This plugin aims to provide a way to send messages at the
        maximum rate allowed by the Discord servers. Using this
        client-side rate limiter will ensure that no messages are
        lost due to increased user activity.
*/

module.exports = bot => {
    const plugin = {};

    //setup-------------------------------------
    plugin.rate = 1000; //message once every x milliseconds
    plugin.queue = {};
    plugin.sendMessage = bot.sendMessage;

    //overrides---------------------------------
    bot.sendMessage = (function() {
        return function() {
            plugin.throttleMessage.apply(bot, arguments);
        };
    })();

    //plugin---------------------------------------
    /**
     * places a message into a sorted outbox to be
     * delivered in order based on the provided
     * time interval
     * @param  {object} msgObj [the discord.io message object]
     */
    plugin.throttleMessage = function(msgObj) {
        const serverID = bot.channels[msgObj.to] ? bot.channels[msgObj.to].guild_id : "DirectMessage";

        //create the queue if it doesn't exist
        if (!plugin.queue[serverID]) {
            plugin.queue[serverID] = {
                running: false,
                outbox: []
            };
        }

        //add the message to the queue outbox
        plugin.queue[serverID].outbox.push(msgObj);

        //start the queue if it isn't running
        if (!plugin.queue[serverID].running) {
            plugin.queue[serverID].running = true;
            plugin.queue[serverID].interval = setInterval(function() {
                //send a message from the outbox if it exists
                if (plugin.queue[serverID].outbox[0]) {
                    plugin.sendMessage.apply(bot, [plugin.queue[serverID].outbox.shift()]);
                } else {
                    clearInterval(plugin.queue[serverID].interval);
                    plugin.queue[serverID].running = false;
                }
            }, plugin.rate);
        }
    };

    //export------------------------------------
    return true;
};