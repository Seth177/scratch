module.exports = function(meta, db, setting) {
    if (setting.add) {
        db.clq({
            type: 'insert',
            location: 'timedelays',
            change: [
                ['channelid', 'commandname', 'length', 'rawtime'],
                [meta.channelID, setting.command, setting.length, Date.now()]
            ]
        });
        db.clq({
            type: 'select',
            what: '*',
            location: 'timedelays',
            id: 'channelid',
            where: meta.channelid
        });
    }
    if (setting.del) {
        db.con("DELETE FROM `timedelays` WHERE `channelid` = '" + meta.channelID + "' AND commandname = '" + setting.commandname + "'");
    }
};