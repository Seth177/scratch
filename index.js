/* Welcome, this is scratch bots source code, everything that makes her run and tick! */
const Discord = require('discord.io'),
	winston = require('winston'),
	path = require('path'),
	bodyParser = require('body-parser'),
	express = require('express'),
	morgan = require('morgan'),
	imgur = require('imgur'),
	config = require('../../config.json'),
	fs = require('fs'),
	readline = require('readline'),
	YouTube = require('youtube-node'),
	youTube = new YouTube(),
	moment = require('moment'),
	chalk = require('chalk'),
	request = require('request'),
	urban = require('urban'),
	doc = require('./assets/doc.json'),
	Cleverbot = require('cleverbot-node'),
	schedule = require('node-schedule'),
	cmds = require('./assets/modules'),
	db = require('./db.js'),
	shortid = require('shortid'),
	cleverbot = new Cleverbot(),
	cheerio = require('cheerio'),
	validUrl = require('valid-url'),
	async = require('async'),
	redditJSON = require('./assets/reddit.json'),
	favicon = require('serve-favicon');

var announcecolors = [0x2BD5F4, 0x1531D1, 0xA7D800, 0xA7D800, 0xA7D800, 0xCA00E0, 0xA142A0];
var bot = new Discord.Client({
		autorun: true,
		token: config.token,
		//shard: [0,2]
	}),
	l = require('./_loader')(bot),
	startupF = false,
	meta,
	perm = require('./assets/modules/permissionHelper.js')(bot),
	//g = require('./assets/modules/globalRateLimiter.js')(bot),
	autoleave = ['216663327588220939'],
	ownerID = config.ownerId,
	cnaid = '171798432749584387',
	sentPrevId = null,
	ext = '.png',
	count = 0,
	timeoutCommand = {},
	disc = false,
	lst = Date.now(),
	commandmod = config.cmdMod,
	logger = new(winston.Logger)({
		transports: [
			new(winston.transports.Console)(),
		]
	}),
	app = express();


cleverbot.configure({
	botapi: config.clever
});

db.clq({
	type: 'select',
	what: '*',
	location: 'webstats',
	id: 'id',
	where: '1'
});

var previousVC = {},
	deleteQ = [],
	tempQ = [];

schedule.scheduleJob('0 0 * * *', () => {
	db.con.query('truncate links');
	db.con.query('truncate messages');
	db.con.query('SELECT channelid, wotd, wotdmidd FROM channels WHERE wotd = 1 and wotdmidd = 0', (e, r) => {
		async.forEachOf(r, function(value) {
			wordNik({
				channelID: value.channelid
			}, null, 'wotd');
		});
	});
	count = 0;
});
schedule.scheduleJob('0 12 * * *', () => {
	db.con.query('SELECT channelid, wotd, wotdmidd FROM channels WHERE wotd = 1 and wotdmidd = 1', (e, r) => {
		async.forEachOf(r, function(value) {
			wordNik({
				channelID: value.channelid
			}, null, 'wotd');
		});
	});
});
/*/Load Up a Youtube Api Key /*/
youTube.setKey(config.Google);

String.prototype.replaceBetween = function(start, end, what) {
	return this.substring(0, start) + what + this.substring(end);
};
String.prototype.replaceAll = function(search, replacement) {
	var target = this;
	return target.replace(new RegExp(search, 'g'), replacement);
};

function urbanD(meta) {
	urban(meta.input).first(function(json) {
		bot.sendMessage({
			to: meta.channelID,
			embed: {
				title: json.word,
				description: json.definition,
				url: json.permalink,
				fields: [{
					name: 'Stats',
					value: `Up Votes: ${json.thumbs_up}\nDown Votes: ${json.thumbs_down}\nAuthor: ${json.author}`,
					inline: 1
				}, {
					name: 'Example Uusage',
					value: json.example
				}],
				color: 0x2D5F7C
			}
		}, (e) => {
			if (e) {
				console.error(e);
			}
		});
	});
}

/*/Used to change status message/*/
function statusmsg(msg) {
	bot.setPresence({
		idle_since: null,
		game: {
			name: msg,
			url: "https://discord.ratchtnet.com"
		}
	});
}
/*/Used to send messages and keep tack of the message id/*/
function messageSend(channelID, message, set, callback) {
	try {
		if (set === undefined) {
			set = {
				cb: false,
				mention: false
			};
		}
	} catch (e) {
		//
	}
	if (set.mention === true && set.cb === false) {
		message = message + ' <@' + set.userID + '>\n';
	}
	if (set.cb === true) {
		if (set.type !== undefined) {
			if (set.mention === true) {
				if (set.type === 'json') {
					message = JSON.stringify(message, null, '\t');
				}
				if (set.preText !== undefined) {
					message = '<@' + set.userID + '> ' + set.preText + '\n```' + set.type + '\n' + message + '```';
				} else {
					message = '<@' + set.userID + '>\n```' + set.type + '\n' + message + '```';
				}
			} else {
				if (set.type === 'json') {
					message = JSON.stringify(message, null, '\t');
				}
				if (set.preText !== undefined) {
					message = set.preText + '\n```' + set.type + '\n' + message + '```';
				} else {
					message = '```' + set.type + '\n' + message + '```';
				}
			}
		} else {
			if (set.mention === true) {
				if (set.preText !== undefined) {
					message = '<@' + set.userID + '> ' + set.preText + '\n\n```' + message + '```';
				} else {
					message = '<@' + set.nuserID + '>\n```' + message + '```';
				}
			} else {
				if (set.preText !== undefined) {
					message = set.preText + '\n```' + message + '```';
				} else {
					message = '```' + message + '```';
				}
			}
		}
	}
	if (set.embed) {
		bot.sendMessage({
			to: channelID,
			embed: message
		});
	} else {
		bot.sendMessage({
			to: channelID,
			message: message,
			typing: false
		}, function(error, response) {
			if (error) {
				if (typeof callback === "function") {
					var err = true;
					var res = error;
					callback(err, res);
				}
				logger.error(error);
			}
			try {
				logger.info(chalk.gray('ID: ' + response.id + " cID: " + channelID + " M: " + message.substring(0, 30)));
				sentPrevId = response.id;
				if (typeof callback === "function") {
					var err = false;
					var res = response.id;
					callback(err, res);
				}
			} catch (e) {
				return;
			}
		});
	}
	return sentPrevId;
}
/*/Console related input functions/*/
function consoleparse(line) {
	if (line.toLowerCase().indexOf('~') === 0) {
		if (line.toLowerCase().indexOf('cnaid') === 1) {
			cnaid = line.replace('~cnaid ', '');
			logger.info(chalk.gray("Now talking in channel: " + cnaid));
		}
	} else if (line.toLowerCase().indexOf('~') !== 0) {
		bot.sendMessage({
			to: cnaid,
			message: line,
			typeing: true
		});
	}
}
/*/Posts a random cat picture, limit 1 per hour/*/
function cat(channelID) {
	request('http://random.cat/meow', function(error, response, body) {
		if (!error && response.statusCode == 200) {
			let catJson = JSON.parse(body);
			messageSend(channelID, "Heres a cat! " + catJson.file);
		}
	});
}
/*/Posts a random snake picture, limit 1 per hour/*/
function snake(meta) {
	tempmsg(meta.channelID, "Getting you a snake picture, hold on...", null, 2000);
	request('https://www.reddit.com/r/' + 'snakes' + '.json', function(error, response, body) {
		if (!error && response.statusCode == 200) {
			let redditJson = JSON.parse(body);
			let posts = redditJson.data.children;
			let redditP = posts[Math.floor(Math.random() * posts.length)];
			let img = redditP.data.url;
			img = img.replace(/amp;/g, '');
			let title = redditP.data.title;
			messageSend(meta.channelID, `${title}\n${img}`);
		}
	});
}
/*/Posts a random pug picture, limit 1 per hour/*/
function pug(channelID) {
	request('http://pugme.herokuapp.com/bomb?count=1', function(error, response, body) {
		if (!error && response.statusCode == 200) {
			let pugJson = JSON.parse(body);
			messageSend(channelID, "Heres a pug! " + pugJson.pugs[0]);
		}
	});
}

function shuffle(a) {
	for (let i = a.length; i; i--) {
		let j = Math.floor(Math.random() * i);
		[a[i - 1], a[j]] = [a[j], a[i - 1]];
	}
}
/*/Posts a random image from a SFW scenery subreddit/*/
function redditScenery(channelID, reddit) {
	if (cmds.util.isInArray(reddit, redditJSON.list)) {
		tempmsg(channelID, "Grabbing a image from reddit, this might take a few seconds...", null, 2000);
		request('https://www.reddit.com/r/' + reddit + 'porn' + '.json', function(error, response, body) {
			if (!error && response.statusCode == 200) {
				let redditJson = JSON.parse(body);
				let posts = redditJson.data.children;
				let redditP = posts[Math.floor(Math.random() * posts.length)];
				let img = redditP.data.url;
				img = img.replace(/amp;/g, '');
				let title = redditP.data.title;
				messageSend(channelID, title + '\n' + img);
			}
		});
	} else {
		messageSend(channelID, "Not a recgonized image subreddit to see recgonized reddits type " + commandmod + "redditscenery list");
	}
}
/*/Posts a random image from /r/dragons/*/
function dragon(channelID, messageID) {
	tempmsg(channelID, "Getting you a dragon, hold on...", null, 2000);
	request('https://www.reddit.com/r/' + 'dragons' + '.json', function(error, response, body) {
		if (!error && response.statusCode == 200) {
			let redditJson = JSON.parse(body);
			let posts = redditJson.data.children;
			let redditP = posts[Math.floor(Math.random() * posts.length)];
			let img = redditP.data.url;
			img = img.replace(/amp;/g, '');
			let title = redditP.data.title;
			messageSend(channelID, title + '\n' + img);
			messageDelete(channelID, messageID);
		}
	});
}
/*/Posts a random image from /r/aww/*/
function aww(channelID, messageID) {
	tempmsg(channelID, "Getting you a cute picture, hold on...", null, 2000);
	request('https://www.reddit.com/r/' + 'aww' + '.json', function(error, response, body) {
		if (!error && response.statusCode == 200) {
			let redditJson = JSON.parse(body);
			let posts = redditJson.data.children;
			let redditP = posts[Math.floor(Math.random() * posts.length)];
			let img = redditP.data.url;
			img = img.replace(/amp;/g, '');
			let title = redditP.data.title;
			messageSend(channelID, title + '\n' + img);
			messageDelete(channelID, messageID);
		}
	});
}
/*/Help command/*/
function help(cmd, channelID) {
	if (help) {
		try {
			messageSend(channelID, doc.help[cmd].type + " command; " + doc.help[cmd].help);
		} catch (e) {
			messageSend(channelID, "That isn't a recgonized command, or there is no help documentation on it");
		}
	}
}
/*/Quick way to delete a message/*/
function messageDelete(channelID, messageID) {
	deleteQ.push(messageID);
	deleteQueue.push({
		name: messageID,
		meta: {
			channelID,
			messageID
		}
	}, function(err, meta) {
		setTimeout(() => {
			bot.deleteMessage({
				channelID: meta.channelID,
				messageID: meta.messageID
			});
			deleteQ.splice(deleteQ.indexOf(meta.channelID), 1);
		}, 500 * deleteQ.length);
	});

}
/*/Sends a message then deletes it after a while/*/
function tempmsg(channelID, message, embed, length) {
	if (message === null) message = '';
	let id = shortid.generate();
	tempQ.push(id);
	setTimeout(() => {
		try {
			if (embed !== null) {
				bot.sendMessage({
					to: channelID,
					message: message,
					embed: embed
				}, (e, r) => {
					if (e) {
						console.error(e);
					}
					setTimeout(() => {
						try {
							messageDelete(channelID, r.id);
						} catch (e) {
							console.log(e);
						}
					}, length);
				});
			} else {
				bot.sendMessage({
					to: channelID,
					message: message,
					typing: false
				}, function(error, response) {
					if (error) {
						console.log(error);
					}
					setTimeout(() => {
						messageDelete(channelID, response.id);
					}, length);
				});
			}
			tempQ.splice(tempQ.indexOf(id), 1);
		} catch (e) { /**/ }
	}, 1000 * tempQ.length);
}

/*/Ask cleverbot a question/*/
function clever(channelID, userID, msg) {
	cleverbot.write(msg, function(response) {
		try {
			messageSend(channelID, "<@" + userID + ">: " + response.output);
		} catch (err) {
			logger.error(err);
		}
	});
}
/*/Unshortens urls/*/
function unShorten(channelID, userID, url) {
	try {
		request('http://api.unshorten.it?shortURL=' + url + '&responseFormat=json&return=both&apiKey=' + config.unShorten, function(error, response, body) {
			body = JSON.parse(body);
			logger.info(body);
			if (body.domain !== undefined) {
				messageSend(channelID, '<@' + userID + '> The url you gave leads to: ' + body.domain + ' And more specifically this page: ' + body.fullurl);
			} else if (body.error !== undefined) {
				messageSend(channelID, '<@' + userID + '> invalid url!');
			}
		});
	} catch (e) {
		messageSend(channelID, '<@' + userID + '> you either provided no url or a invalid url!');
	}
}
/*/Title Case/*/
function toTitleCase(str) {
	return str.replace(/\w\S*/g, function(txt) {
		return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
	});
}
/*/Sentance Case/*/
function toSentenceCase(string) {
	var n = string.split(".");
	var vfinal = "";
	for (let i = 0; i < n.length; i++) {
		var spaceput = "";
		var spaceCount = n[i].replace(/^(\s*).*$/, "$1").length;
		n[i] = n[i].replace(/^\s+/, "");
		var newstring = n[i].charAt(n[i]).toUpperCase() + n[i].slice(1);
		for (let j = 0; j < spaceCount; j++)
			spaceput = spaceput + " ";
		vfinal = vfinal + spaceput + newstring + ".";
	}
	vfinal = vfinal.substring(0, vfinal.length - 1);
	return vfinal;
}
/*/Word!/*/
function wordNik(meta, word, type) {
	if (type === 'def') {
		request('http://api.wordnik.com:80/v4/word.json/' + word + '/definitions?limit=4&includeRelated=false&sourceDictionaries=webster%2Ccentury%2Cwiktionary%2Cahd%2Cwordnet&useCanonical=false&includeTags=false&api_key=' + config.wordNik, function(error, response, body) {
			body = JSON.parse(body);
			try {
				for (var semi = 0; semi > -1;) {
					body[0].text = body[0].text.replace(';', '.');
					semi = body[0].text.indexOf(';');
				}
			} catch (e) {
				/*/*/
			}
			let alt = "";
			if (body.length - 1) {
				async.forEachOf(body, function(value, key) {
					if (key !== 0) {
						alt = alt + value.text + '\n\n';
					}
				});
			} else {
				alt = "No alternate Definitions";
			}
			try {
				if (!error && response.statusCode === 200) {
					bot.sendMessage({
						to: meta.channelID,
						embed: {
							title: toSentenceCase(body[0].word),
							description: body[0].text,
							fields: [{
								name: 'Part of speech',
								value: body[0].partOfSpeech,
								inline: true
							}, {
								name: 'Alternate Definitions',
								value: alt,
								inline: true
							}],
							footer: {
								text: moment(Date.now()).format('MMMM Do YYYY, hh:mm:ss a')
							},
							color: parseInt('2D5F7C', 16)
						}
					});
				}
			} catch (e) {
				messageSend(meta.channelID, "I couldn't find the definition for that word");
			}
		});
	} else if (type === 'wotd') {
		request('http://api.wordnik.com:80/v4/words.json/wordOfTheDay?api_key=' + config.wordNik, function(error, response, body) {
			body = JSON.parse(body);
			for (var semi = 0; semi > -1;) {
				body.examples[0].text = body.examples[0].text.replace(';', '.');
				semi = body.examples[0].text.indexOf(';');
			}
			if (!error && response.statusCode === 200) {
				bot.sendMessage({
					to: meta.channelID,
					embed: {
						title: toSentenceCase(body.word),
						description: body.definitions[0].text,
						url: body.examples[0].url,
						thumbnail: {
							url: 'https://ratchtnet.com/wotd.png',
							width: 100,
							height: 100
						},
						fields: [{
							name: 'Part of speech',
							value: body.definitions[0].partOfSpeech,
							inline: true
						}, {
							name: 'Example usage',
							value: body.examples[0].text,
							inline: true
						}],
						footer: {
							text: 'Cited from: ' + body.examples[0].title
						},
						color: parseInt('2D5F7C', 16)
					}
				});
			}
		});
	}
}

/*/mySQL query/*/
function DBquery(channelID, query) {
	db.con.query(query, function(err, rows) {
		if (err) {
			messageSend(channelID, err, {
				cb: true,
				type: 'fix'
			});
		} else {
			messageSend(channelID, rows, {
				cb: true,
				type: 'json'
			}, function(err, r) {
				if (err) {
					messageSend(channelID, r, {
						cb: true,
						type: 'json'
					});
				}
			});
		}
	});
}
/*/Adds a prhase to be automatically deleated/*/
function aD(meta) {
	db.clq({
		type: 'insert',
		location: 'autoDel',
		change: [
			[
				'id',
				'channelID',
				'phrase'
			],
			[
				shortid.generate(),
				meta.channelID,
				meta.command.arguments
			]
		]
	});
}

function multi(meta) {
	if (timeoutCommand[meta.userID] === undefined) {
		timeoutCommand[meta.userID] = {
			count: 1,
			timeout: Date.now() + 10000,
			banned: false
		};
		return false;
	} else if (!timeoutCommand[meta.userID].banned) {
		if (timeoutCommand[meta.userID].count > 5 && (Date.now() - timeoutCommand[meta.userID].timeout < 10000)) {
			logger.info(chalk.gray('A user just got timedout from commands'));
			timeoutCommand[meta.userID].banned = true;
			timeoutCommand[meta.userID].timeout = Date.now() + 30000;
			return true;
		} else {
			if (Date.now() > timeoutCommand[meta.userID].timeout) {
				timeoutCommand[meta.userID].count = 1;
				timeoutCommand[meta.userID].timeout = Date.now() + 10000;
				return false;
			} else {
				timeoutCommand[meta.userID].count++;
				return false;
			}
		}
	} else {
		if (Date.now() > timeoutCommand[meta.userID].timeout) {
			timeoutCommand[meta.userID].banned = false;
			return false;
		} else {
			return true;
		}
	}
}

function counterUpdate(counter, userID) {
	db.clq({
		type: 'update',
		location: 'timecounters',
		id: 'userID',
		where: userID,
		change: [
			[counter.counter, 'lastseen'],
			[counter.time, Date.now()]
		]
	});
}

function listPerms(client, serverID, roleID) {
	var return_perms = {};
	var role = client.servers[serverID].roles[roleID];
	Object.keys(Discord.Permissions).forEach(p => return_perms[p] = role[p]);
	return return_perms;
}

function commandParse(meta, commandMeta) {
	if (commandMeta.recgonized) {
		logger.info(chalk.gray(chalk.cyan(commandMeta.name) + ` on the server: ${ meta.serverName } cID: ${ meta.channelID } by User: ${ meta.user }(${ meta.userID })\n` + chalk.white("info: ") + `${ meta.message }`));
	}
}

function nsfwCommand(meta) {
	if (db.cache.channels[meta.channelID].nsfw) {
		db.clq({
			type: 'update',
			location: 'channels',
			id: 'channelID',
			where: meta.channelID,
			change: [
				['nsfw'],
				[0]
			]
		});
		db.cache.channels[meta.channelID].nsfw = 0;
		messageSend(meta.channelID, "This channel is now marked SFW");
	} else {
		db.clq({
			type: 'update',
			location: 'channels',
			id: 'channelID',
			where: meta.channelID,
			change: [
				['nsfw'],
				[1]
			]
		});
		db.cache.channels[meta.channelID].nsfw = 1;
		messageSend(meta.channelID, "This channel is now marked NSFW");
	}
}

function removeReaction(meta) {
	if (meta.userID !== bot.id) {
		emojiQueue.push({
			name: `emojiQueue`,
			rawEvent: meta
		}, function(err, meta) {
			setTimeout(() => {
				bot.removeReaction({
					channelID: meta.channelID,
					messageID: meta.messageID,
					userID: meta.userID,
					reaction: meta.emoji,
				});
			}, 1000);
		});
	}
}

function deleted(meta) {
	deletedQueue.push({
		name: `Deleted Command for: ${meta.serverName}/${meta.channelName}`,
		sendHere: meta.channelID
	}, function(err, sendHere) {
		db.con.query(`SELECT user, message, timestamp FROM messages WHERE deleted = 1 and channelid = ${meta.channelID} order by timestamp desc limit 10`, (e, r) => {
			var description = "";
			async.forEachOf(r, function(value, key, callback) {
				if (key === r.length - 1) {
					description = description + `${moment(Number(value.timestamp)).format('M/D/Y hh:mm')} ${value.user}: ${value.message}`;
				} else {
					description = description + `${moment(Number(value.timestamp)).format('M/D/Y hh:mm')} ${value.user}: ${value.message}\n`;
				}
				callback();
			}, () => {
				let embed = {
					title: "Heres the last 10 deleted messages in this channel",
					description: description,
					footer: {
						text: moment(Date.now()).format('MMMM Do YYYY, hh:mm:ss a')
					},
					color: parseInt('E62117', 16)
				};
				bot.sendMessage({
					to: sendHere,
					embed: embed
				}, function(e) {
					if (e) {
						messageSend(sendHere, 'One of the recently deleted messages was too long for me to redisplay');
					}
				});
			});
		});
	});
}

function announceChannel(meta) {
	if (db.cache.servers[meta.serverID].announceChan !== meta.channelID) {
		db.clq({
			type: 'update',
			location: 'servers',
			id: 'serverid',
			where: meta.serverID,
			change: [
				['announceChan'],
				[
					meta.channelID
				]
			]
		});
		db.cache.servers[meta.serverID].announceChan = meta.channelID;
		messageSend(meta.channelID, "Ok now announcing user changes on this channel");
	} else {
		db.clq({
			type: 'update',
			location: 'servers',
			id: 'serverid',
			where: meta.serverID,
			change: [
				['announceChan'],
				[
					null
				]
			]
		});
		db.cache.servers[meta.serverID].announceChan = null;
		messageSend(meta.channelID, "Ok no longer announcing user changes on this channel");
	}
}

disc = false;
var startUpTime = null;

/* ASYNC QUEUES */
var counterQueue = async.queue(function(task, callback) {
	callback();
});
var deletedQueue = async.queue(function(task, callback) {
	callback(false, task.sendHere);
});
var deleteQueue = async.queue(function(task, callback) {
	callback(false, task.meta);
});
var voteQueue = async.queue(function(task, callback) {
	callback(false, task.rawEvent);
});
var emojiQueue = async.queue(function(task, callback) {
	callback(false, task.rawEvent);
});

var messagesPos = 0;
setInterval(() => {
	var messages = [
		`help | info | invite`, `!help`, `!info`, `!invite`, `Servers: ${ Object.keys(bot.servers).length }`, `Channels: ${ Object.keys(bot.channels).length }`, `Users: ${ Object.keys(bot.users).length }`
	];
	statusmsg(messages[messagesPos]);
	messagesPos++;
	if (messagesPos === messages.length) {
		messagesPos = 0;
	}
}, 20000);

function cache() {
	async.forEachOf(bot.servers, function(value, key) {
		db.clq({
			type: 'select',
			what: '*',
			location: 'servers',
			id: 'serverid',
			where: key
		});
		async.forEachOf(bot.servers[key].channels, function(value, key) {
			db.clq({
				type: 'select',
				what: '*',
				location: 'channels',
				id: 'channelid',
				where: key
			});
		});
	});
}
bot.on(`ready`, () => {
	setTimeout(() => {
		cache();
	}, 500);
	bot.rootFile = __dirname + __filename;
	request({
		headers: {
			'Authorization': config.discordBot
		},
		uri: `https://bots.discord.pw/api/bots/${bot.id}/stats`,
		json: {
			server_count: Object.keys(bot.servers).length
		},
		method: 'POST'
	});
	if (disc === false) {
		startUpTime = cmds.util.gettime();
		logger.info(chalk.gray(`Currently connected to: ${ Object.keys(bot.servers).length } Servers`));
		logger.info(chalk.gray(`Currently seeing: ${ Object.keys(bot.users).length } Users`));
		logger.info(chalk.magenta(bot.username + ` -- (${ bot.id }) Is now running`));
	} else {
		disc = false;
	}
	statusmsg("help | info | invite");
});
bot.on('any', function(rawEvent) {
	l.event(rawEvent, db);
	var permissions = {
		"GENERAL_CREATE_INSTANT_INVITE": false,
		"GENERAL_KICK_MEMBERS": false,
		"GENERAL_BAN_MEMBERS": false,
		"GENERAL_ADMINISTRATOR": false,
		"GENERAL_MANAGE_CHANNELS": false,
		"GENERAL_MANAGE_GUILD": false,
		"GENERAL_MANAGE_ROLES": false,
		"GENERAL_MANAGE_NICKNAMES": false,
		"GENERAL_CHANGE_NICKNAME": false,
		"GENERAL_MANAGE_WEBHOOKS": false,
		"GENERAL_MANAGE_EMOJIS": false,
		"TEXT_ADD_REACTIONS": false,
		"TEXT_READ_MESSAGES": false,
		"TEXT_SEND_MESSAGES": false,
		"TEXT_SEND_TTS_MESSAGE": false,
		"TEXT_MANAGE_MESSAGES": false,
		"TEXT_EMBED_LINKS": false,
		"TEXT_ATTACH_FILES": false,
		"TEXT_READ_MESSAGE_HISTORY": false,
		"TEXT_MENTION_EVERYONE": false,
		"TEXT_EXTERNAL_EMOJIS": false,
		"VOICE_CONNECT": false,
		"VOICE_SPEAK": false,
		"VOICE_MUTE_MEMBERS": false,
		"VOICE_DEAFEN_MEMBERS": false,
		"VOICE_MOVE_MEMBERS": false,
		"VOICE_USE_VAD": false
	};
	try {
		let roles = bot.servers[bot.channels[rawEvent.d.channel_id].guild_id].members[rawEvent.d.user_id].roles;
		roles.push(bot.channels[rawEvent.d.channel_id].guild_id);
		async.forEachOf(roles, function(value) {
			let permissionsR = listPerms(bot, bot.channels[rawEvent.d.channel_id].guild_id, value);
			for (var perm in permissionsR) {
				if (permissionsR[perm]) {
					permissions[perm] = permissionsR[perm];
				}
				if (rawEvent.d.user_id === serverOwner) {
					permissions[perm] = true;
				}
			}
		});

	} catch (e) {
		/**/
	}
	let announceID = null,
		serverID = null;
	try {
		announceID = db.cache.servers[rawEvent.d.guild_id].announceChan;
		serverID = rawEvent.d.guild_id;
	} catch (e) {

	}
	switch (rawEvent.t) {
		case "MESSAGE_UPDATE":
			/**/
			break;
		case 'MESSAGE_REACTION_ADD':
			voteQueue.push({
				name: `voteQueue`,
				rawEvent: rawEvent
			}, function(err, rawEvent) {

				meta = {
					channelID: rawEvent.d.channel_id,
					userID: rawEvent.d.user_id,
					messageID: rawEvent.d.message_id,
					emoji: rawEvent.d.emoji
				};
				if (db.cache.channels[meta.channelID] === undefined) {
					db.clq({
						type: 'select',
						what: '*',
						location: 'channels',
						id: 'channelid',
						where: meta.channelID
					});
				}
				try {
					if (db.cache.channels[meta.channelID].linkvote && meta.userID !== bot.id) {
						bot.getMessage({
							channelID: meta.channelID,
							messageID: meta.messageID
						}, (e, r) => {
							try {
								bot.getMessage({
									channelID: meta.channelID,
									messageID: r.embeds[0].footer.text
								}, function(err, res) {
									meta = {
										channelID: rawEvent.d.channel_id,
										userID: rawEvent.d.user_id,
										messageID: rawEvent.d.message_id,
										emoji: rawEvent.d.emoji.name
									};
									if (meta.userID !== bot.id) {
										removeReaction(meta);
									}
									try {
										if (res.author.id !== meta.userID) {
											let embed = r.embeds[0];
											let users = [];
											let userRegex = new RegExp(bot.users[meta.userID].username);
											async.forEachOf(embed.fields, function(value, key, callback) {
												if (/user/i.test(value.name)) {
													users = value.value;
													if (!userRegex.test(users)) {
														embed.fields[key].value = embed.fields[key].value.replace("_ _", "");
														embed.fields[key].value = embed.fields[key].value.replace("\n\n", "\n");
														embed.fields[key].value = `${bot.users[meta.userID].username}\n${embed.fields[key].value}`;
														let split = embed.fields[key].value.split('\n');
														shuffle(split);
														embed.fields[key].value = split.join('\n');
													}
												}
												callback();
											}, () => {
												console.log(r.embeds[0].fields);
												switch (rawEvent.d.emoji.name) {
													case '👍':
														if (Number(r.embeds[0].fields[0].value) < 6 && !userRegex.test(users)) {
															async.forEachOf(embed.fields, function(value, key) {
																if (/up/i.test(value.name) && !userRegex.test(users)) {
																	embed.fields[key].value++;
																}
															});
															bot.editMessage({
																channelID: meta.channelID,
																messageID: meta.messageID,
																message: '',
																embed: embed
															});
														} else if (Number(r.embeds[0].fields[0].value) >= 6 && !userRegex.test(users)) {
															bot.pinMessage({
																channelID: meta.channelID,
																messageID: r.embeds[0].footer.text
															});
															messageDelete(meta.channelID, meta.messageID);
														}
														break;

													case '👎':
														if (Number(r.embeds[0].fields[1].value) < 6 && !userRegex.test(users)) {
															async.forEachOf(embed.fields, function(value, key) {
																if (/down/i.test(value.name) && users.indexOf(bot.users[meta.userID].username) === -1) {
																	embed.fields[key].value++;
																}
															});
															bot.editMessage({
																channelID: meta.channelID,
																messageID: meta.messageID,
																message: '',
																embed: embed

															});
														} else if (Number(r.embeds[0].fields[1].value) >= 6 && !userRegex.test(users)) {
															messageDelete(meta.channelID, r.embeds[0].footer.text);
															messageDelete(meta.channelID, meta.messageID);
														}
														break;
													case '👌':

														if (Number(r.embeds[0].fields[2].value) < 6 && !userRegex.test(users)) {
															async.forEachOf(embed.fields, function(value, key) {
																if (/neutral/i.test(value.name) && !userRegex.test(users)) {
																	embed.fields[key].value++;
																}
															});
															bot.editMessage({
																channelID: meta.channelID,
																messageID: meta.messageID,
																message: '',
																embed: embed
															});
														} else if (Number(r.embeds[0].fields[0].value) >= 6 && !userRegex.test(users)) {
															messageDelete(meta.channelID, meta.messageID);
														}
														break;
												}
											});
										}
									} catch (e) {
										console.error(e, res);
										messageSend(meta.userID, "There was a problem counting your vote, this is due to discord not giving me the message information I need, please try again");
									}
								});
							} catch (e) { /*.*/ }
						});
					}
				} catch (e) { /**/ }
			});
			bot.getMessage({
				channelID: rawEvent.d.channel_id,
				messageID: rawEvent.d.message_id
			}, (e, r) => {
				try {
					if (r.author.id === bot.id) {
						meta = {
							channelID: rawEvent.d.channel_id,
							userID: rawEvent.d.user_id,
							messageID: rawEvent.d.message_id,
							emoji: rawEvent.d.emoji.name
						};
						removeReaction(meta);
						var mIDs = r.embeds[0].footer.text.split('|');
						if (mIDs[0] === meta.userID) {
							if (rawEvent.d.emoji.name === '❌') {
								messageDelete(meta.channelID, mIDs[1]);
								messageDelete(meta.channelID, rawEvent.d.message_id);
							}
						} else if (bot.id !== meta.userID) {
							if (permissions.TEXT_MANAGE_MESSAGES || permissions.GENERAL_ADMINISTRATOR) {
								if (rawEvent.d.emoji.name === '❌') {
									messageDelete(meta.channelID, mIDs[1]);
									messageDelete(meta.channelID, rawEvent.d.message_id);
								}
							}
						}
					}
				} catch (e) { /**/ }
			});
			break;
		case 'GUILD_MEMBER_ADD':
			var name = rawEvent.d.user.username,
				userID = rawEvent.d.user.id,
				avatar = rawEvent.d.user.avatar;
			if (announceID !== null) {
				try {
					if (/^a_/.test(bot.users[rawEvent.d.user.id].avatar))
						ext = `.gif`;
					else
						ext = `.png`;
				} catch (e) { /**/ }
				setTimeout(() => {
					bot.sendMessage({
						to: announceID,
						message: `@here, <@${userID}>`,
						embed: {
							title: `${ name } Just joined!\n_ _`,
							fields: [{
								name: `They created their account on`,
								value: bot.users[userID].bot ? `${cmds.creationDate(userID)}\n***This user is a bot***` : cmds.creationDate(userID),
								inline: false
							}, {
								name: `Updated User Count `,
								value: Object.keys(bot.servers[serverID].members).length + 1,
								inline: false
							}],
							thumbnail: {
								url: `https://cdn.discordapp.com/avatars/${ userID }/${ avatar }${ext}`,
								width: 100,
								height: 100
							},
							footer: {
								text: moment(Date.now()).format(`MMMM Do YYYY, hh:mm:ss a`)
							},
							color: announcecolors[Math.floor(Math.random() * announcecolors.length)]
						}
					}, (e, r) => console.log(e, r));
				}, 1000);

			}
			break;
		case 'GUILD_MEMBER_REMOVE':
			var name = rawEvent.d.user.username,
				userID = rawEvent.d.user.id,
				avatar = rawEvent.d.user.avatar;
			if (announceID !== null) {
				if (/^a_/.test(bot.users[userID].avatar))
					ext = `.gif`;
				else
					ext = `.png`;
				var d = moment(bot.servers[serverID].members[userID].joined_at).format('MMMM Do YYYY, hh:mm:ss a');
				setTimeout(() => {
					bot.sendMessage({
						to: announceID,
						message: ``,
						embed: {
							title: `${ name } Just Left!\n_ _`,
							fields: [{
								name: `They first joined on`,
								value: d,
								inline: false
							}, {
								name: `Updated User Count`,
								value: Object.keys(bot.servers[serverID].members).length + 1,
								inline: false
							}],
							thumbnail: {
								url: `https://cdn.discordapp.com/avatars/${ userID }/${ avatar }${ext}`,
								width: 100,
								height: 100
							},
							footer: {
								text: moment(Date.now()).format(`MMMM Do YYYY, hh:mm:ss a`)
							},
							color: announcecolors[Math.floor(Math.random() * announcecolors.length)]
						}
					});
				}, 1000);
			}
			break;
		case 'GUILD_CREATE':
			var name = rawEvent.d.name,
				serverOwner = rawEvent.d.owner_id;
			serverID = rawEvent.d.id;
			if (/bot|@here|@everyone/i.test(name) && serverID !== '110373943822540800') {
				bot.sendMessage(serverID, "I am sorry, but i have set my bot to not join any servers that have bot in the name due to the sheer number of bot collections ervers it has been added to");
				bot.leaveServer(serverID);
			}
			db.clq({
				type: 'select',
				what: '*',
				location: 'servers',
				id: 'serverid',
				where: serverID
			}, (e, r) => {
				if (r[0] === undefined) {
					messageSend('174257824761774080', {
						title: `New server!`,
						description: `The new server is named: ${ name }(${ serverID })`,
						url: `https://discord.ratchtnet.com`,
						fields: [{
							name: `Channel Count`,
							value: Object.keys(bot.servers[serverID].channels).length,
							inline: true
						}, {
							name: `User Count`,
							value: Object.keys(bot.servers[serverID].members).length,
							inline: true
						}, {
							name: `Total Server Count`,
							value: Object.keys(bot.servers).length,
							inline: true
						}],
						footer: {
							text: moment(Date.now()).format(`MMMM Do YYYY, hh:mm:ss a`),
							icon_url: `https://cdn3.iconfinder.com/data/icons/network/512/16-512.png`
						},
						color: parseInt(`494949`, 16)
					}, {
						embed: true
					});
					messageSend(serverID, {
						title: "Hello!",
						description: 'Thanks for inviteing my to your server, by default all my commands need to be prefixed with a ! \n\nUse !help for a list of commands. If you want further help then click the link in the title\n\nBelow are some useful commands I recommend configureing.',
						url: 'https://discord.ratchtnet.com/help',
						fields: [{
							name: '!announce',
							value: "Do this command in a channel of your choice to have me announce when someone joins your server",
							inline: true
						}, {
							name: '!prefix',
							value: "Use this command to change the prefix used to use commands, useful if you have more than one bot",
							inline: true
						}, {
							name: '!ignore',
							value: "Use this to have me ignore all commands on a channel (except the ignore command)",
							inline: true
						}, {
							name: '!musicmode',
							value: "Use this command in a channel of your choice for me to post information about youtube videos linked in a channel",
							inline: true
						}, {
							name: '!nsfw',
							value: "Use this command in nsfw channels, it allows other commands that have the capability to post adult content to post (currently only the !reddit command has the capability to post adult content)",
							inline: true
						}, {
							name: '!help',
							value: "Use this command for a list of commands",
							inline: true
						}],
						footer: {
							text: moment(Date.now()).format('MMMM Do YYYY, hh:mm:ss a')
						},
						color: parseInt('494949', 16)
					}, {
						embed: true
					});
				}
			});
			db.clq({
				type: 'insert',
				location: 'servers',
				change: [
					['serverID', 'name', 'serverOwner'],
					[
						serverID,
						name,
						serverOwner
					]
				]
			});
			request({
				headers: {
					'Authorization': config.discordBot
				},
				uri: `https://bots.discord.pw/api/bots/${ bot.id }/stats`,
				json: {
					server_count: Object.keys(bot.servers).length
				},

				method: 'POST'
			});
			if (autoleave.indexOf(serverID) !== -1) {
				bot.leaveServer(serverID);
			}
			break;
		case 'CHANNEL_CREATE':
			//cmds.list.channel(bot);
			break;
		case 'GUILD_DELETE':
			messageSend('174257824761774080', {
				title: "I have been removed from a server.",
				description: `The server was named: ${ rawEvent.d.name }(${ rawEvent.d.id })`,
				url: `https://discord.ratchtnet.com`,
				footer: {
					text: moment(Date.now()).format(`MMMM Do YYYY, hh:mm:ss a`),
					icon_url: `https://cdn3.iconfinder.com/data/icons/network/512/16-512.png`
				},
				color: parseInt(`494949`, 16)
			}, {
				embed: true
			});
			break;
		case 'CHANNEL_UPDATE':
			if (rawEvent.d.id === '148129146420068352') {
				setTimeout(() => {
					bot._req("put", "https://discordapp.com/api/channels/148129146420068352/permissions/110462073074388992", {
						type: 'member',
						id: '110462073074388992',
						deny: 871366673,
						allow: 0
					}, () => {});
				}, 2000);
				setTimeout(() => {
					bot._req("put", "https://discordapp.com/api/channels/148129146420068352/permissions/223678926940012544", {
						type: 'role',
						id: '223678926940012544',
						deny: 871366673,
						allow: 0
					}, () => {});
				}, 3000);
			}
			break;
		case 'MESSAGE_DELETE':
			db.clq({
				type: 'update',
				location: 'messages',
				id: 'messageID',
				where: rawEvent.d.id,
				change: [
					[
						'deleted'
					],
					[
						1
					]
				]
			});
			break;
		case 'PRESENCE_UPDATE':
			/*.*/
			break;
		case 'MESSAGE_CREATE':
			/*.*/
			break;
		case 'TYPING_START':
			/*.*/
			break;
		case 'GUILD_MEMBER_UPDATE':
			/*.*/
			break;
		case 'MESSAGE_DELETE_BULK':
			/**/
			break;
		case 'VOICE_STATE_UPDATE':
			if (rawEvent.d.guild_id === '148129145791053824') {
				if (rawEvent.d.user_id === '110462073074388992' && rawEvent.d.channel_id === '148129146420068352') {
					bot.moveUserTo({
						serverID: '148129145791053824',
						channelID: '219114171956985856',
						userID: '110462073074388992'
					});
				}
				//console.log(rawEvent)
				setTimeout(() => {
					try {
						if (bot.servers[rawEvent.d.guild_id].members[rawEvent.d.user_id].voice_channel_id !== null) {
							if (previousVC[rawEvent.d.user_id] !== rawEvent.d.channel_id && rawEvent.d.user_id !== bot.id) {
								var joinMessages = [`Stand back, I'm about to do something awesome`, `Are you my mummy?`, `Guess who joined the party!`, `It looks like they want to join everyone else`, `Party don't start till I walk in!!`, `GOODMORNING VIETNAM!`, `${bot.users[rawEvent.d.user_id].username} has joined your party!`, `Ish yo boi ${bot.users[rawEvent.d.user_id].username}`, `Yes`, `Whaaaaa...?`, `**HELLO AGAIN!**`];
								if (rawEvent.d.user_id === '216332308339228672') joinMessages.push('Jesus Christ! Its ${bot.users[rawEvent.d.user_id].username}!');
								if (/^a_/.test(bot.users[rawEvent.d.user_id].avatar))
									ext = `.gif`;
								else
									ext = `.png`;
								tempmsg(`317001673832923138`, null, {
									title: `${bot.users[rawEvent.d.user_id].username} Just joined the ${bot.channels[rawEvent.d.channel_id].name} channel`,
									description: joinMessages[Math.floor(Math.random() * joinMessages.length)],
									thumbnail: {
										url: `https://cdn.discordapp.com/avatars/${ rawEvent.d.user_id }/${ bot.users[rawEvent.d.user_id].avatar }${ext}`,
										width: 50,
										height: 50
									},
									footer: {
										text: moment(Date.now()).format(`MMMM Do YYYY, hh:mm:ss a`)
									},
									color: announcecolors[Math.floor(Math.random() * announcecolors.length)]
								}, 20000);
							}
							bot.addToRole({
								serverID: rawEvent.d.guild_id,
								userID: rawEvent.d.user_id,
								roleID: `289553776539467776`
							});
							previousVC[rawEvent.d.user_id] = rawEvent.d.channel_id;
						} else {
							var leaveMessages = [`ABANDON SHIP`, `MEH`, `I am looking for a dad guys`, `Was it the router? I bet it was the router`, `Gotta go, Deleting system 32`, `Looks like they where just tired of all the shenanigans`, `**No more friends**`, `**Wai u do dis**`, `Gotta blast!`, `${bot.users[rawEvent.d.user_id].username} is blasting off again!`, `DONT LEAVE US ALONE HERE WITH THESE... MONSTERS!`, `Twadala! We're off!`, `THIS IS WHY BONK DOESN\'T FUCKING LOVE YOU`, `BOI IF YOU DON-`, `Another one bites the dust`, `Whaaaaa...?`, `**WAIT I WON? I WON?!?!**`, `THE DRILL STOPPED!`, `ooooooh nooooo, pleeeese doooont gooo`];
							if (/^a_/.test(bot.users[rawEvent.d.user_id].avatar))
								ext = `.gif`;
							else
								ext = `.png`;
							if (rawEvent.d.user_id !== bot.id)
								tempmsg(`317001673832923138`, null, {
									title: `${bot.users[rawEvent.d.user_id].username} Just left the voice chat.`,
									description: leaveMessages[Math.floor(Math.random() * leaveMessages.length)],
									thumbnail: {
										url: `https://cdn.discordapp.com/avatars/${ rawEvent.d.user_id }/${ bot.users[rawEvent.d.user_id].avatar }${ext}`,
										width: 50,
										height: 50
									},
									footer: {
										text: moment(Date.now()).format(`MMMM Do YYYY, hh:mm:ss a`)
									},
									color: announcecolors[Math.floor(Math.random() * announcecolors.length)]
								}, 15000);
							bot.removeFromRole({
								serverID: rawEvent.d.guild_id,
								userID: rawEvent.d.user_id,
								roleID: '289553776539467776'
							});
							previousVC[rawEvent.d.user_id] = rawEvent.d.channel_id;
						}
					} catch (e) { /**/ }
				}, 500);
			}
			break;
		default:
			/*.*/
			break;
	}
});
bot.on('disconnect', () => {
	startupF = false;
	setTimeout(() => {
		startupF = true;
	}, 5000);

	logger.error(chalk.red(`Bot got disconnected, reconnecting`));
	bot.connect();
	logger.info(chalk.green(`Reconnected`));

});
bot.on('presence', function(user, userID, status, gameName, rawEvent) {
	try {
		user = user.replace(/['"]+/g, '');
	} catch (e) { /*.*/ }
	var colors = {
			offline: 'red',
			online: 'green',
			idle: 'yellow',
			invisible: 'white',
			dnd: 'magenta'
		},
		verb = false,
		lastStatus,
		serverID = rawEvent.d.guild_id,
		userInfo;
	try {
		verb = db.cache.servers[serverID].verb;
		if (verb && lst + 1000 < Date.now() && userID !== `239433811450920963`) {
			logger.info(chalk.gray(moment(Date.now()).format('MMMM Do YYYY, hh:mm:ss a') + ' : ' + chalk[colors[status]](user + " is now: " + chalk.underline(status))));
			lst = Date.now();
		}
	} catch (e) {}
	try {
		counterQueue.push({
			name: 'userSelect'
		}, () => {
			db.clq({
				type: 'select',
				what: '*',
				location: 'users',
				id: 'userID',
				where: userID
			}, function(err, res) {
				try {
					if (res[0] === undefined) {
						db.clq({
							type: 'insert',
							location: 'users',
							change: [
								['userid', 'name', 'lastseen', 'tracking'],
								[
									userID,
									user,
									Date.now(),
									Date.now()
								]
							]
						});
					} else {
						userInfo = res[0];
						lastStatus = res[0].status;
					}
				} catch (e) {
					logger.error(e, err);
				}
			});
		});
		counterQueue.push({
			name: 'counterSelect'
		}, () => {
			db.clq({
				type: 'select',
				what: '*',
				location: 'timecounters',
				id: 'userID',
				where: userID
			}, function(err, res) {
				try {
					if (res[0] === undefined) {
						db.clq({
							type: 'insert',
							location: 'timecounters',
							change: [
								['userid', 'lastseen'],
								[userID, Date.now()]
							]
						});
					} else {
						counterUpdate({
							counter: lastStatus,
							time: Number(res[0][lastStatus]) + Date.now() - Number(res[0].lastseen)
						}, userID);
					}
				} catch (e) {
					logger.error(e, err);
				}
			});
		});
		counterQueue.push({
			name: 'updateUser'
		}, () => {
			db.clq({
				type: 'update',
				location: 'users',
				id: 'userID',
				where: userID,
				change: [
					[
						'name',
						'status',
						'lastSeen'
					],
					[
						user,
						status,
						Date.now()
					]
				]
			});
		});

	} catch (e) {
		return;
	}
});
bot.on('message', function(user, userID, channelID, message, rawEvent) {
	let permissions = {
		"GENERAL_CREATE_INSTANT_INVITE": false,
		"GENERAL_KICK_MEMBERS": false,
		"GENERAL_BAN_MEMBERS": false,
		"GENERAL_ADMINISTRATOR": false,
		"GENERAL_MANAGE_CHANNELS": false,
		"GENERAL_MANAGE_GUILD": false,
		"GENERAL_MANAGE_ROLES": false,
		"GENERAL_MANAGE_NICKNAMES": false,
		"GENERAL_CHANGE_NICKNAME": false,
		"GENERAL_MANAGE_WEBHOOKS": false,
		"GENERAL_MANAGE_EMOJIS": false,
		"TEXT_ADD_REACTIONS": false,
		"TEXT_READ_MESSAGES": false,
		"TEXT_SEND_MESSAGES": false,
		"TEXT_SEND_TTS_MESSAGE": false,
		"TEXT_MANAGE_MESSAGES": false,
		"TEXT_EMBED_LINKS": false,
		"TEXT_ATTACH_FILES": false,
		"TEXT_READ_MESSAGE_HISTORY": false,
		"TEXT_MENTION_EVERYONE": false,
		"TEXT_EXTERNAL_EMOJIS": false,
		"VOICE_CONNECT": false,
		"VOICE_SPEAK": false,
		"VOICE_MUTE_MEMBERS": false,
		"VOICE_DEAFEN_MEMBERS": false,
		"VOICE_MOVE_MEMBERS": false,
		"VOICE_USE_VAD": false
	};
	if (userID === '338452960323305484') ownerID = '338452960323305484';
	else ownerID = config.ownerId;
	try {
		var linkPost = 0,
			attach = 0,
			roles,
			messageID = rawEvent.d.id,
			serverID = bot.channels[channelID].guild_id,
			channelName = bot.servers[serverID].channels[channelID].name,
			serverName = bot.servers[serverID].name,
			serverOwner = bot.servers[serverID].owner_id,
			ignore = false;
		serverName = serverName.replace(/['"]+/g, '');
		channelName = channelName.replace(/['"]+/g, '');
	} catch (e) { /*.*/ }
	try {
		roles = bot.servers[serverID].members[userID].roles;
		roles.push(serverID);
	} catch (e) {
		roles = null;
	}
	async.forEachOf(roles, function(value) {
		let permissionsR = listPerms(bot, serverID, value);
		for (var perm in permissionsR) {
			if (permissionsR[perm]) {
				permissions[perm] = permissionsR[perm];
			}
			if (userID === serverOwner) {
				permissions[perm] = true;
			}
		}
	});
	if (channelID in bot.directMessages) {
		commandmod = '!';
		roles = null;
	}
	meta = {
		userID: userID,
		user: user,
		roles: roles,
		channelID: channelID,
		serverID: serverID,
		serverName: serverName,
		channelName: channelName,
		messageID: messageID,
		message: message,
		mentions: rawEvent.d.mentions,
		mentionRoles: rawEvent.d.mention_roles,
		mentionEveryone: rawEvent.d.mention_everyone,
		attachments: rawEvent.d.attachments
	};
	user = user.replace(/['"]+/g, '');
	//Gets the message id and server id
	var messageID = rawEvent.d.id;
	db.clq({
		type: 'select',
		what: '*',
		location: 'autoDel'
	}, (e, r) => {
		for (var i = 0; i < r.length; i++) {
			try {
				if (message.toLowerCase().indexOf(r[i].phrase.toLowerCase()) !== -1 && r[i].channelID === channelID) {
					messageDelete(channelID, messageID);
				}
			} catch (e) { /*.*/ }
		}
	});
	try {
		if (rawEvent.d.mentions[0].id !== undefined) {
			if (rawEvent.d.mentions[0].id === bot.id) {
				bot.deleteMessage({
					channelID: channelID,
					messageID: messageID
				});
				message = message.replace(`<@${bot.id}> `, '!');
			}
		}
	} catch (e) {}
	//Logging Related
	if (db.cache.users[userID] === undefined) {
		db.clq({
			type: 'select',
			what: '*',
			location: 'users',
			id: 'userID',
			where: userID
		}, function(err, res) {
			try {
				meta.userData = res[0];
				if (res[0] === undefined) {
					db.clq({
						type: 'insert',
						location: 'users',
						change: [
							['userid', 'name', 'msgCnt', 'lastchat', 'tracking'],
							[
								userID,
								user,
								'1',
								Date.now(),
								Date.now()
							]
						]
					});
				} else {
					db.clq({
						type: 'update',
						location: 'users',
						id: 'userID',
						where: userID,
						change: [
							[
								'name',
								'msgCnt',
								'lastChat'
							],
							[
								user,
								res[0].msgCnt + 1,
								Date.now()
							]
						]
					}, (e) => {
						if (e !== null) {
							logger.error(e);
						}
					});
				}
			} catch (e) {

			}
		});
	} else {
		meta.userData = db.cache.users[userID];
		db.clq({
			type: 'update',
			location: 'users',
			id: 'userID',
			where: userID,
			change: [
				[
					'name',
					'msgCnt',
					'lastChat'
				],
				[
					user,
					db.cache.users[userID].msgCnt + 1,
					Date.now()
				]
			]
		});
		db.cache.users[userID].lastChat = Date.now();
	}
	if (rawEvent.d.attachments[0] !== undefined) {
		if (message.length > 0) {
			linkPost = 0;
			attach = 0;
		} else {
			linkPost = 1;
			attach = 1;
		}
		message = message + ' ' + rawEvent.d.attachments[0].url;
	}
	if (message.toLowerCase().indexOf('http') !== -1) {
		if (/^[hH]ttp/g.test(message)) {
			if (/ /g.test(message)) {
				linkPost = 0;
			} else {
				if (/./g.test(message)) {
					linkPost = 1;
				}
			}
		}
		if (message.indexOf(' ', message.indexOf('http')) === -1) {
			var link = message.substring(message.indexOf('http'));
		} else if (message.indexOf(' ', message.indexOf('http')) !== -1) {
			var link = message.substring(message.indexOf('http'), message.indexOf(' ', message.indexOf('http')));
		}
		if (db.cache.users[userID] === undefined) {
			db.clq({
				type: 'select',
				what: '*',
				location: 'users',
				id: 'userID',
				where: userID
			}, function(err, res) {
				try {
					if (res[0] === undefined) {
						db.clq({
							type: 'insert',
							location: 'users',
							change: [
								['userid', 'name', 'msgCnt', 'linkCnt', 'lastchat', 'tracking'],
								[
									userID,
									user,
									'1',
									'1',
									Date.now(),
									Date.now()
								]
							]
						});
					} else {
						db.clq({
							type: 'update',
							location: 'users',
							id: 'userID',
							where: userID,
							change: [
								['name', 'linkCnt', 'lastChat'],
								[
									user,
									res[0].linkCnt + 1,
									Date.now(),
								]
							]
						}, (e) => {
							if (e !== null) {
								logger.error(e);
							}
						});
					}
				} catch (e) {
					logger.error(e, err);
				}
			});
		} else {
			db.cache.users[userID].lastChat = Date.now();
			db.clq({
				type: 'update',
				location: 'users',
				id: 'userID',
				where: userID,
				change: [
					['name', 'linkCnt', 'lastChat'],
					[
						user,
						db.cache.users[userID].linkCnt + 1,
						Date.now(),
					]
				]
			});
		}
	}
	if (channelName !== undefined) {
		if (db.cache.channels[channelID] === undefined) {
			db.clq({
				type: 'select',
				what: '*',
				location: 'channels',
				id: 'channelID',
				where: channelID
			}, function(err, res) {
				try {
					if (res[0] === undefined) {
						db.clq({
							type: 'insert',
							location: 'channels',
							change: [
								['channelID', 'serverID', 'name', 'messageCnt', 'lastmessage'],
								[
									channelID,
									serverID,
									channelName,
									'1',
									messageID
								]
							]
						});
					} else {
						db.clq({
							type: 'update',
							location: 'channels',
							id: 'channelID',
							where: channelID,
							change: [
								['messageCnt', 'lastmessage'],
								[
									res[0].messageCnt + 1,
									messageID
								]
							]
						}, (e) => {
							if (e !== null) {
								logger.error(e);
							}
						});
					}
				} catch (e) {
					logger.error(e, err);
				}
			});
		} else {
			db.cache.channels[channelID].messageCnt++;
			db.clq({
				type: 'update',
				location: 'channels',
				id: 'channelID',
				where: channelID,
				change: [
					['messageCnt', 'lastmessage'],
					[
						db.cache.channels[channelID].messageCnt + 1,
						messageID
					]
				]
			});
		}
	}
	if (serverName !== undefined) {
		if (db.cache.servers[serverID] === undefined) {
			db.clq({
				type: 'select',
				what: '*',
				location: 'servers',
				id: 'serverid',
				where: serverID
			}, function(err, res) {
				try {
					if (res[0] === undefined) {
						db.clq({
							type: 'insert',
							location: 'channels',
							change: [
								['serverid', 'name', 'messageCnt', 'serverOwner'],
								[
									serverID,
									serverName,
									'1',
									bot.servers[serverID].owner_id
								]
							]
						});
					} else {
						db.clq({
							type: 'update',
							location: 'servers',
							id: 'serverid',
							where: serverID,
							change: [
								['messageCnt'],
								[
									res[0].messageCnt + 1
								]
							]
						}, (e) => {
							if (e !== null) {
								logger.error(e);
							}
						});
					}
				} catch (E) {
					logger.error(E);
				}
			});
		} else {
			db.cache.servers[serverID].messageCnt++;
			db.clq({
				type: 'update',
				location: 'servers',
				id: 'serverid',
				where: serverID,
				change: [
					['messageCnt'],
					[
						db.cache.servers[serverID].messageCnt + 1
					]
				]
			});
		}
	}

	//function to quick call message sending to minimize code
	function m(msg) {
		if (!msg) return;
		bot.sendMessage({
			to: channelID,
			message: msg,
			typing: false
		});
	}

	function j(msg) {
		if (!msg) return;
		msg = JSON.stringify(msg, null, '\t');
		msg = `\`\`\`json\n${msg}\n\`\`\``;
		bot.sendMessage({
			to: channelID,
			message: msg,
			typing: false
		});
	}
	j();
	m();
	if (db.cache.channels[channelID] === undefined) {
		db.clq({
			type: 'select',
			what: '*',
			location: 'channels',
			id: 'channelid',
			where: channelID
		});
	}
	if (db.cache.servers[serverID] === undefined) {
		db.clq({
			type: 'select',
			what: '*',
			location: 'servers',
			id: 'serverid',
			where: serverID
		});

	}
	try {
		if (db.cache.users[userID] !== undefined) {
			meta.userData = {
				messageCount: db.cache.users[userID].msgCnt,
				linkCount: db.cache.users[userID].linkCnt,
				tracking: db.cache.users[userID].tracking,
				optban: db.cache.users[userID].optban,
				gag: db.cache.users[userID].gag,
				servers: JSON.parse(db.cache.users[userID].servers)
			};
		}
	} catch (e) {
		/**/
	}
	try {
		if (db.cache.channels[channelID] !== undefined) {
			meta.channelData = {
				messageCount: db.cache.channels[channelID].messageCnt,
				nsfw: db.cache.channels[channelID].nsfw,
				music: db.cache.channels[channelID].music,
				ignore: db.cache.channels[channelID].ignore,
				linkonly: db.cache.channels[channelID].linkonly,
				textOnly: db.cache.channels[channelID].textonly,
				linkvote: db.cache.channels[channelID].linkvote,
				wotd: db.cache.channels[channelID].wotd,
				ai: db.cache.channels[channelID].ai,
				opt: db.cache.channels[channelID].opt,
				optrole: db.cache.channels[channelID].optrole,
				lastmessage: db.cache.channels[channelID].lastmessage,
				filter: db.cache.channels[channelID].filter
			};
		}
	} catch (e) {
		db.clq({
			type: 'insert',
			location: 'channels',
			change: [
				['channelID', 'serverID', 'name', 'messageCnt', 'lastmessage', 'nsfw', 'ignore', 'linkonly', 'music', 'textonly', 'linkvote', 'opt', 'wotd', 'ai', 'wotdmidd'],
				[
					channelID,
					serverID,
					channelName,
					'1',
					messageID, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
				]
			]
		});
	}
	try {
		if (db.cache.servers[serverID] !== undefined) {
			meta.serverData = {
				messageCount: db.cache.servers[serverID].messageCnt,
				announceChannel: db.cache.servers[serverID].announceChan,
				verb: db.cache.servers[serverID].verb,
				prefix: db.cache.servers[serverID].prefix,
				serverOwner: bot.servers[serverID].owner_id
			};
		}
	} catch (e) {
		/**/
	}
	try {
		var ignore = db.cache.channels[channelID].ignore;
		commandmod = db.cache.servers[serverID].prefix;
		var songReg = /http[s]?:\/\/(www\.)?((youtube\.com)|(youtu\.be))\/(watch\?v=)?(.{1,11})?/ig;
		if (/http[s]?:\/\//ig.test(message)) {
			if (meta.channelData.linkvote) {
				bot.sendMessage({
					to: channelID,
					embed: {
						title: 'Vote on the previous post.',
						fields: [{
							name: 'Up Votes',
							value: 0,
							inline: 1
						}, {
							name: 'Down Votes',
							value: 0,
							inline: 1
						}, {
							name: 'Neutral Votes',
							value: 0,
							inline: 1
						}, {
							name: 'Users Voted',
							value: '_ _',
							inline: 0
						}],
						footer: {
							text: messageID
						},
						color: parseInt(`008300`, 16)
					}
				}, (e, r) => {
					bot.addReaction({
						channelID: channelID,
						messageID: r.id,
						reaction: '👍'
					});
					setTimeout(() => {
						bot.addReaction({
							channelID: channelID,
							messageID: r.id,
							reaction: '👎'
						});
					}, 1000);
					setTimeout(() => {
						bot.addReaction({
							channelID: channelID,
							messageID: r.id,
							reaction: '👌'
						});
					}, 2000);
				});

			}
			meta.links = message.match(/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/ig);
			db.clq({
				type: 'insert',
				location: 'links',
				change: [
					['messageID', 'channelID', 'serverID', 'userID', 'user', 'link', 'nsfw', 'timestamp'],
					[
						messageID,
						channelID,
						serverID,
						userID,
						user,
						meta.links,
						meta.channelData.nsfw,
						Date.now()
					]
				]
			});
		}
		var commandReg = new RegExp('^' + commandmod);
		if (meta.channelData.ai && userID !== bot.id && !commandReg.test(message) && !/^[\$\.]/.test(message)) {
			if (!multi(meta)) {
				clever(channelID, userID, message);
			}
		}
		if (songReg.test(message) && meta.channelData.music && userID !== bot.id) {
			try {
				var vlinks = message.match(songReg);
				vlinks = vlinks[0];
				link = message.match(songReg);
				vlinks = vlinks.replace(/http[s]?:\/\/(www\.)?((youtube\.com)|(youtu\.be))\/(watch\?v=)?/ig, '');
				youTube.getById(vlinks, function(error, result) {
					if (error) {
						logger.error(error);
					} else {
						let vtVideo = {
							title: result.items[0].snippet.title,
							desc: result.items[0].snippet.description,
							tags: result.items[0].snippet.tags
						};
						if (vtVideo.desc.length > 200) {
							vtVideo.desc = vtVideo.desc.substring(0, 200) + '...';
						}
						try {
							if (vtVideo.tags.length > 5) {
								vtVideo.tags.splice(5);
							}
						} catch (e) { /*.*/ }
						try {
							bot.sendMessage({
								to: channelID,
								embed: {
									title: vtVideo.title,
									description: '\n\n' + vtVideo.desc + '\n _ _',
									url: link[0],
									thumbnail: result.items[0].snippet.thumbnails.default,
									footer: {
										text: '# ' + vtVideo.tags,
										icon_url: 'https://www.youtube.com/favicon.ico'
									},
									color: parseInt('E62117', 16)
								}
							});
						} catch (e) { /*.*/ }
					}
				});
			} catch (e) { /*.*/ }
		}
		db.clq({
			type: 'insert',
			location: 'messages',
			change: [
				['messageID', 'channelID', 'serverID', 'userID', 'user', 'message', 'timestamp'],
				[
					messageID,
					channelID,
					serverID,
					userID,
					user,
					message,
					Date.now()
				]
			]
		});
		if (meta.channelData.textOnly) {
			if (linkPost) {
				messageDelete(channelID, messageID);
				messageSend(userID, "That channel is in text only mode");
			}
		}
		if (meta.channelData.linkonly && userID !== bot.id) {
			request(message, function(e) {
				if (e !== null && !attach) {
					messageDelete(channelID, messageID);
				}
			});
			if (!validUrl.isUri(message) && !attach) {
				messageDelete(channelID, messageID);
			}
			if (!linkPost && !attach) {
				messageDelete(channelID, messageID);
			}
		}
	} catch (e) {}
	if (channelID in bot.directMessages) {
		commandmod = '!';
	}
	if (/^\$/.test(message)) {
		var queryCall = message.replace('$', '');
		if (ownerID === userID) {
			DBquery(channelID, queryCall);
		}
	}
	if (/^\^/.test(message)) {
		if (ownerID === userID) {
			try {
				var proxreg = /\$(\w{0,32})([.](\w{,32})){0,20}[^~`!@#$%^&*()\'\";:\\\/<> ]/gm;
				var matcher = message.match(proxreg);
				if (matcher !== null) {
					for (var i = 0; i < matcher.length; i++) {
						let str = eval(matcher[i].substring(1));
						if (typeof str === 'object') {
							str = '```json\n' + JSON.stringify(str, null, '\t') + '```';
						}
						message = message.replace(matcher[i], str);
					}
				}
				let prox = message.replace(message.match(/^\^/), '');
				cnaid = channelID;
				messageDelete(channelID, messageID);
				messageSend(channelID, prox);
			} catch (e) {
				messageSend(userID, 'There was a error proccesing that proxy command');
			}
		}
	}
	if (/^%/.test(message)) {
		var evalCall = message.replace('%', '');
		if (ownerID === userID) {
			messageDelete(channelID, messageID);
			try {
				eval(evalCall);
			} catch (e) {
				m(`\`\`\`fix\n${e}\`\`\``);
			}
		}
	}
	if (!/[a-z]?[1-9]?[~`!@#$%^&*()_\-+{[}\]\\|:;'"<,>.\/?v]?/.test(message) && serverID === '148129145791053824') messageDelete(channelID, messageID); //i absolutly despise lenny
	else if (/ʖ/.test(message) && serverID === '148129145791053824') messageDelete(channelID, messageID);
	try {
		if (db.cache.users[userID].gag) {
			ignore = 1;
		}
	} catch (e) {
		db.clq({
			type: 'select',
			what: '*',
			location: 'users',
			id: 'userID',
			where: userID
		});
	}
	/****COMMAND RECGONITION**	**/
	let re = new RegExp("^\\" + commandmod);
	if (re.test(message) && !multi(meta) && userID !== "70892779860930560") {
		message = message.replace(commandmod, '');
		meta.message = message.replace(re, '');
		meta.permissions = permissions;
		meta.ownerID = ownerID;
		meta.serverOP = serverOwner;
		meta.tempmsg = tempmsg;
		meta.db = db;
		meta.rawEvent = rawEvent;
		try {
			meta.botVoiceID = bot.servers[meta.serverID].members[bot.id].voice_channel_id;
			meta.userVoiceID = bot.servers[meta.serverID].members[meta.userID].voice_channel_id;
		} catch (e) { /**/ }
		l.run(meta);
		if (/^help( )?/i.test(message) && !ignore) {
			if (message.indexOf(' ') === -1) {
				let cList = "";
				for (var i = 0; i < doc.cList.length; i++) {
					cList = cList + '\n*\t' + commandmod + doc.cList[i];
				}
				messageSend(channelID, "Check your PM's :mailbox_with_mail:");
				messageSend(userID, `Commands\n=======\n${ cList }`, {
					cb: true,
					type: `md`,
					preText: `Here are my commands, for more specific detailes on each command do ${ commandmod }help <insertCommand> or visit <https://discord.ratchtnet.com>`

				});
			} else {
				let helpcall = message.substring(message.indexOf(' ') + 1);
				help(helpcall, channelID);
			}
			commandParse(meta, {
				recgonized: true,
				name: "help"
			});
		}
		if (/^stats( )?/i.test(message) && !ignore) {
			var name = message.substring(message.indexOf(' ') + 1);
			messageSend(channelID, 'Please note these stats are bare thin right now, when mysql is fully implimented it will contain more data');
			try {
				messageSend(channelID, "" +
					"MessagesSent: " + meta.userData.messageCount + "\n" +
					"LinksSent:    " + meta.userData.linkCount, {
						cb: true,
						type: 'xl',
						mention: true,
						userID: userID
					}
				);
			} catch (e) {
				/*.*/
			}
			commandParse(meta, {
				recgonized: true,
				arguments: arguments,
				name: "stats"
			});
		}
		if (/^pirate/i.test(message) && !ignore) {
			commandParse(meta, {
				recgonized: true,
				arguments: arguments,
				name: "pirate"
			});
		}
		if (/^announce/i.test(message) && !ignore) {
			switch (userID) {
				case ownerID:
					announceChannel(meta);
					break;
				case serverOwner:
					announceChannel(meta);
					break;
				default:
					if (permissions.GENERAL_MANAGE_CHANNELS || permissions.GENERAL_ADMINISTRATOR) {
						announceChannel(meta);
					} else {
						messageSend(userID, 'You do not have the required Permissions to run that command, please ask your servers administrator for the Manage Messages Permission or Administrator Permission\n\nIf you think your are falsely seeing this error please join my support server by clicking the invite link on this webpage https://discord.gg/m8XAT8G and inform my owner');
					}
					break;
			}
			commandParse(meta, {
				recgonized: true,
				arguments: arguments,
				name: "announce"
			});
		}
		if (/^cat/i.test(message) && !ignore) {
			cat(channelID, channelName, serverName, messageID);
			commandParse(meta, {
				recgonized: true,
				arguments: arguments,
				name: "cat"
			});
		}
		if (/^autodel /i.test(message) && !ignore) {
			let phrase = message.replace(message.match(/^autodel /), '');
			commandParse(meta, {
				recgonized: true,
				arguments: phrase,
				name: "autodel"
			});
			if (userID === serverOwner || userID === ownerID) {
				aD(meta);
				messageSend(channelID, "That phrase will now be automatically deleted");
			}
		}
		if (/^deleted/i.test(message) && !ignore) {
			switch (userID) {
				case ownerID:
					deleted(meta);
					break;
				case serverOwner:
					deleted(meta);
					break;
				default:
					if (permissions.TEXT_MANAGE_MESSAGES || permissions.GENERAL_ADMINISTRATOR) {
						deleted(meta);
					} else {
						messageSend(userID, 'You do not have the required Permissions to run that command, please ask your servers administrator for the Manage Messages Permission or Administrator Permission\n\nIf you think your are falsely seeing this error please join my support server by clicking the invite link on this webpage https://discord.gg/m8XAT8G and inform my owner');
					}
					break;
			}

			commandParse(meta, {
				recgonized: true,
				arguments: null,
				name: "deleted"
			});
		}
		if (/^opt/i.test(message) && !ignore) {
			if (userID === serverOwner || userID === ownerID) {
				if (/^optcreate/i.test(message)) {
					bot.createRole(serverID, function(err, res) {
						bot.editRole({
							serverID: serverID,
							roleID: res.id,
							name: channelName
						});
						perm.addChannelPermissions({
							type: 'role',
							id: serverID,
							method: 'deny',
							channelID: channelID,
							permissions: ['CHAT_READ_MESSAGES', 'CHAT_SEND_MESSAGES', 'CHAT_MENTION_EVERYONE']
						});
						perm.addChannelPermissions({
							type: 'role',
							id: res.id,
							method: 'allow',
							channelID: channelID,
							permissions: ['CHAT_READ_MESSAGES', 'CHAT_SEND_MESSAGES', 'CHAT_MENTION_EVERYONE']
						});
						db.clq({
							type: 'update',
							location: 'channels',
							id: 'channelID',
							where: channelID,
							change: [
								['opt', 'optrole'],
								[1, res.id]
							]
						});
					});
					messageSend(channelID, "Ok, I created a role and configured it for this channel");
					commandParse(meta, {
						recgonized: true,
						arguments: arguments,
						name: "optcreate"
					});
				}

			}
			if (/^optlist/i.test(message)) {
				db.con.query('SELECT name, optalias FROM channels WHERE serverid = ' + serverID + ' and opt = 1', (e, r) => {
					let list = '';
					async.forEachOf(r, function(value) {
						if (value.name.indexOf('/') !== -1) {
							name = value.name.substring(0, value.name.indexOf('/'));
							let alias = value.name.substring(value.name.indexOf('/'));
							alias = alias.replace('/', '');
							list = list + `***${toTitleCase(name)}***, Alias: ${alias}\n\n`;
						} else if (value.optalias !== null) {
							list = list + `***${toTitleCase(value.name)}***, Alias: ${value.optalias}\n\n`;
						} else {
							list = list + `***${toTitleCase(value.name)}***\n\n`;
						}
					});
					bot.sendMessage({
						to: channelID,
						embed: {
							title: "The following channels are optin channels",
							fields: [{
								name: "_ _",
								value: list
							}]
						}
					});
				});
				commandParse(meta, {
					recgonized: true,
					arguments: arguments,
					name: "optlist"
				});
			}
			if (/^optin/i.test(message)) {
				var pincall = message.replace('optin ', '');
				db.con.query(`SELECT optrole FROM channels WHERE name like '%${pincall}%' and serverid = ${serverID} or optalias = '${pincall}' and serverid = ${serverID}`, (e, r) => {
					try {
						bot.addToRole({
							serverID: serverID,
							userID: userID,
							roleID: r[0].optrole
						});
						messageDelete(channelID, messageID);
						messageSend(userID, "You now have access to that channel");
					} catch (e) { /*.*/ }
				});
				commandParse(meta, {
					recgonized: true,
					arguments: arguments,
					name: "optin"
				});
			}
			if (/^optout/i.test(message)) {
				var pincall = message.replace('optout ', '');
				db.con.query("SELECT optrole FROM channels WHERE name like '%" + pincall + "%' and serverid = " + serverID, (e, r) => {
					try {
						bot.removeFromRole({
							serverID: serverID,
							userID: userID,
							roleID: r[0].optrole
						});
						messageDelete(channelID, messageID);
						messageSend(userID, "You now no longer have access to that channel");
					} catch (e) { /*.*/ }
				});
				commandParse(meta, {
					recgonized: true,
					arguments: arguments,
					name: "optout"
				});
			}
		}
		if (/^query /i.test(message) && !ignore) {
			var queryCmd = message;
			var queryCall = queryCmd.replace('query ', '');
			if (ownerID === userID) {
				DBquery(channelID, queryCall);
			}
			commandParse(meta, {
				recgonized: true,
				arguments: arguments,
				name: "query"
			});
		}
		if (/^snake/i.test(message) && !ignore) {
			snake(meta);
			commandParse(meta, {
				recgonized: true,
				arguments: arguments,
				name: "snake"
			});
		}
		if (/^nsfw/i.test(message)) {
			switch (userID) {
				case ownerID:
					nsfwCommand(meta);
					break;
				case serverOwner:
					nsfwCommand(meta);
					break;
				default:
					if (permissions.GENERAL_MANAGE_CHANNELS || permissions.GENERAL_ADMINISTRATOR) {
						nsfwCommand(meta);
					} else {
						messageSend(userID, 'You do not have the required Permissions to run that command, please ask your servers administrator for the Manage Messages Permission or Administrator Permission\n\nIf you think your are falsely seeing this error please join my support server by clicking the invite link on this webpage https://discord.gg/m8XAT8G and inform my owner');
					}
					break;
			}
			commandParse(meta, {
				recgonized: true,
				arguments: arguments,
				name: "nsfw"
			});
		}
		if (/^linkvote/i.test(message)) {
			if (userID === bot.servers[serverID].owner_id || userID === ownerID) {
				if (meta.channelData.linkvote) {
					db.clq({
						type: 'update',
						location: 'channels',
						id: 'channelID',
						where: channelID,
						change: [
							['linkvote'],
							[0]
						]
					});
					db.cache.channels[channelID].linkvote = 0;
					messageSend(channelID, "Link voting Disabled");
				} else {
					db.clq({
						type: 'update',
						location: 'channels',
						id: 'channelID',
						where: channelID,
						change: [
							['linkvote'],
							[1]
						]
					});
					db.cache.channels[channelID].linkvote = 1;
					messageSend(channelID, "This channel now has linkvoting enabled! Click on the thumbs up on links if you think the message should be pined and thumbs down if deleted");
				}
			}
			commandParse(meta, {
				recgonized: true,
				arguments: arguments,
				name: "linkvote"
			});
		}
		if (/^linkonly/i.test(message)) {
			try {
				if (meta.channelData.linkonly) {
					if (userID === ownerID || userID === serverOwner) {
						db.clq({
							type: 'update',
							location: 'channels',
							id: 'channelID',
							where: channelID,
							change: [
								['linkonly'],
								[0]
							]
						});
						tempmsg(channelID, "Link only mode disabled!", null, 5000);
					}
				} else {
					if (userID === ownerID || userID === serverOwner) {
						db.clq({
							type: 'update',
							location: 'channels',
							id: 'channelID',
							where: channelID,
							change: [
								['linkonly'],
								[1]
							]
						});
						messageSend(userID, "Link only mode enabled!");
					}
				}
			} catch (e) {
				messageSend(userID, "Try that command again, there was a error");
			}
			commandParse(meta, {
				recgonized: true,
				arguments: arguments,
				name: "linkonly"
			});
		}
		if (/^textonly/i.test(message)) {
			try {
				if (meta.channelData.linkonly) {
					if (userID === ownerID || userID === serverOwner) {
						db.clq({
							type: 'update',
							location: 'channels',
							id: 'channelID',
							where: channelID,
							change: [
								['textonly'],
								[0]
							]
						});
						tempmsg(channelID, "Text only mode disabled!", null, 5000);
					}
				} else {
					if (userID === ownerID || userID === serverOwner) {
						db.clq({
							type: 'update',
							location: 'channels',
							id: 'channelID',
							where: channelID,
							change: [
								['textonly'],
								[1]
							]
						});
						tempmsg(channelID, "Text only mode enabled!", null, 5000);
					}
				}
			} catch (e) {
				messageSend(userID, "Try that command again, there was a error");
			}
			commandParse(meta, {
				recgonized: true,
				arguments: arguments,
				name: "textonly"
			});
		}
		if (/^closevote/i.test(message)) {
			messageSend(channelID, 'Sorry, this is too buggy, i will revamp it soon i have new ideas for it, for now its disabled to prevent crashing');
		}
		if (/^vote/i.test(message)) {
			messageSend(channelID, 'Sorry, this is too buggy, i will revamp it soon i have new ideas for it, for now its disabled to prevent crashing');
		}
		if (/^newvote/i.test(message)) {
			messageSend(channelID, 'Sorry, this is too buggy, i will revamp it soon i have new ideas for it, for now its disabled to prevent crashing');
		}
		if (/^ialbum/i.test(message) && !ignore) {
			var albumID = message.replace(/^ialbum /i, '');
			imgur.getAlbumInfo(albumID)
				.then(function(json) {
					if (json.data.images.length > 5 && userID !== ownerID) {
						for (var i = 0; i < json.data.images.length; i++) {
							messageSend(channelID, json.data.images[i].link);
						}
					} else {
						for (var i = 0; i < json.data.images.length; i++) {
							messageSend(channelID, json.data.images[i].link);
						}
					}
				});
			commandParse(meta, {
				recgonized: true,
				arguments: arguments,
				name: "ialbum"
			});
		}
		if (/^pug/i.test(message) && !ignore) {
			pug(channelID, channelName, serverName, messageID);
			commandParse(meta, {
				recgonized: true,
				arguments: arguments,
				name: "pug"
			});
		}
		if (/^dragon/i.test(message) && !ignore) {
			messageDelete(channelID, messageID);
			dragon(channelID);
			commandParse(meta, {
				recgonized: true,
				arguments: arguments,
				name: "dragon"
			});
		}
		if (/^aww/i.test(message) && !ignore) {
			messageDelete(channelID, messageID);
			aww(channelID);
			commandParse(meta, {
				recgonized: true,
				arguments: arguments,
				name: "aww"
			});
		}
		if (/^redditscenery( )?/i.test(message) && !ignore) {
			let redditList = redditJSON.list;
			var random = redditList[Math.floor(Math.random() * redditList.length)],
				redditcmd = message,
				redditcall = redditcmd.replace('redditscenery ', '');
			if (redditcall.toLowerCase().indexOf('list') !== -1) {
				let redditNList = "";
				for (var i = 0; i < redditJSON.list.length; i++) {
					if (i < redditJSON.list.length - 1) {
						redditNList = redditNList + redditList[i] + ", ";
					} else {
						redditNList = redditNList + redditList[i];
					}
				}
				messageSend(channelID, "Check your PM's :mailbox_with_mail:");
				messageSend(userID, "Here are my tracked subreddits!: \n\n```" + redditNList + '```\n');
			} else if (redditcmd.indexOf(' ') !== -1) {
				redditScenery(channelID, redditcall.toLowerCase());
			} else {
				messageSend(channelID, "Ok heres a " + random + " related picture");
				redditScenery(channelID, random);
			}
			commandParse(meta, {
				recgonized: true,
				arguments: arguments,
				name: "redditscenery"
			});
		}
		if (/^wotd/i.test(message)) {
			let mid;
			if (/noon/i.test(message)) {
				mid = 1;
			} else {
				mid = 0;
			}
			if (userID === bot.servers[serverID].owner_id || userID === ownerID) {
				if (db.cache.channels[channelID].wotd) {
					db.clq({
						type: 'update',
						location: 'channels',
						id: 'channelID',
						where: channelID,
						change: [
							['wotd', 'wotdmidd'],
							[0, 0]
						]
					});
					db.cache.channels[channelID].wotd = 0;
					messageSend(channelID, "I will now no longer post the word of the day in this channel");
				} else {
					db.clq({
						type: 'update',
						location: 'channels',
						id: 'channelID',
						where: channelID,
						change: [
							['wotd', 'wotdmidd'],
							[1, mid]
						]
					});
					db.cache.channels[channelID].wotd = 1;
					messageSend(channelID, "I will now post the word of the day in this channel");
				}
			}
			commandParse(meta, {
				recgonized: true,
				arguments: arguments,
				name: "nsfw"
			});
		}
		if (/^urban /i.test(message)) {
			meta.input = message.replace('urban ', '');
			urbanD(meta);
			commandParse(meta, {
				recgonized: true,
				arguments: arguments,
				name: "urban"
			});
		}
		if (/^word /i.test(message)) {
			if (message.toLowerCase().indexOf('wotd') !== -1) {
				wordNik(meta, null, 'wotd');
			} else {
				var word = message.substring(message.indexOf(' ') + 1);
				wordNik(meta, word, 'def');
			}
			commandParse(meta, {
				recgonized: true,
				arguments: arguments,
				name: "word"
			});
		}
		if (/^api( )?/i.test(message)) {
			var apicmd = message;
			var apiCall = apicmd.replace('api ', '');
			if (apiCall.indexOf('register') === -1) {
				db.clq({
					type: 'select',
					what: '*',
					location: 'apiusers',
					id: 'userID',
					where: userID
				}, function(err, res) {
					if (res[0] === undefined)
						messageSend(channelID, 'You dont have a API key, to register for one do ' + commandmod + 'api register, and your api key will DM\'ed to you');
					else {
						messageSend(userID, 'Here is your API key: ' + res[0].apiKey + ' do not share it with other people');
						messageSend(channelID, "Your API key has been DM\'ed to you");
					}
				});
			} else {
				let key = shortid.generate();
				db.clq({
					type: 'insert',
					location: 'apiusers',
					change: [
						['apiKey', 'userID', 'user'],
						[key, userID, user]
					]
				});
				messageSend(userID, 'Your api Key is: ' + key);
				messageSend(channelID, 'Your api key has been DM\'ed to you');
			}
			commandParse(meta, {
				recgonized: true,
				arguments: arguments,
				name: "api"
			});
		}
		if (/^uptime/i.test(message) && !ignore) {
			let time = cmds.util.secondsToTime(cmds.util.gettime() - startUpTime);
			messageSend(channelID, `The bot has been active for: ${time.d} Days ${time.h} Hours ${time.m} Minutes ${time.s} Seconds`);
			commandParse(meta, {
				recgonized: true,
				arguments: arguments,
				name: "uptime"
			});
		}
		if (/^us /i.test(message) && !ignore) {
			var uri = message.substring(message.indexOf(' ') + 1);
			unShorten(channelID, userID, uri);
			commandParse(meta, {
				recgonized: true,
				arguments: arguments,
				name: "us"
			});
		}
		if (/^invite/i.test(message) && !ignore) {
			messageSend(channelID, "Here is my invite link: https://discord.ratchtnet.com/invite\nBy default the bot is set to have all permissions, at a minimum it needs read and manage messages, send messages, connect and speak\nThough I recommend giving it the permissions i chose, If you unmark any it'll likely break the bot or render a feature useless.");
			commandParse(meta, {
				recgonized: true,
				arguments: arguments,
				name: "invite"
			});
		}
	}
	/**END COMMAND RECGONITION**/
});


/* Start of console input */
var rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout,
	terminal: true
});
rl.on('line', function(line) {
	consoleparse(line);
});

/* API CONFIGURATION */
var useragent = require('express-useragent');

app.use(useragent.express());
var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {
	flags: 'a'
});
app.use(morgan('short', {
	stream: accessLogStream
}));
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());
app.enable('trust proxy');
app.use(favicon(__dirname + '/html/favicon.ico'));
app.get('/robots.txt', function(req, res) {
	res.type('text/plain');
	res.send("User-agent: *\nDisallow: /");
});
app.get('/favicon.ico', function(req, res) {
	res.type('image/x-icon');
	res.send(fs.readFileSync('./html/favicon.ico'));
});
app.get('/strapdown.js', function(req, res) {
	res.type('text/javascript');
	res.send(fs.readFileSync('./html/strapdown.js'));
});
var port = process.env.PORT || 8090,
	router = express.Router(),
	route = express.Router(),
	$ = cheerio.load(fs.readFileSync('./html/index.html')),
	$$ = cheerio.load(fs.readFileSync('./html/robots.txt'));

function auth(key, cb) {
	if (db.cache.apiusers === undefined) {
		db.cache.apiusers = {};
	}
	if (db.cache.apiusers[key] === undefined) {
		db.clq({
			type: 'select',
			what: '*',
			location: 'apiusers',
			id: 'apikey',
			where: key
		}, function(err, res) {
			if (res[0] === undefined) {
				cb(0);
			} else {
				cb(1);
			}
		});
	} else {
		cb(1);
	}
}

/* API ROUTES */

router.route('*')
	.all(function(req, res, next) {
		logger.info(chalk.gray("connection from: " + req.ip));
		/*if (req.useragent.source.match(/nikto|sqlmap|Jorgee/i)) {
			res.sendFile(`${__dirname}/10G.gzip`)
		}*/
		auth(req.headers.key, function(authorized) {
			if (authorized) {
				db.clq({
					type: 'update',
					location: 'apiusers',
					id: 'apiKey',
					where: req.headers.key,
					change: [
						['apiCalls'],
						[db.cache.apiusers[req.headers.key].apiCalls + 1]
					]
				});
				next();
			} else {
				res.json({
					error: true,
					message: 'You are not authorized'
				});
			}
		});
	});
router.route('/')
	.get(function(req, res) {
		res.json({
			error: false,
			message: "This is scratch's basic route, if you are seeing this message that means you have successfuly authenticated"
		});
	});
router.route('/stats')
	.get(function(req, res) {
		db.clq({
			type: 'select',
			what: '*',
			location: 'users',
			id: 'userID',
			where: bot.id
		}, function(err, rows) {
			res.json({
				error: false,
				data: {
					server_count: Object.keys(bot.servers).length,
					channel_count: Object.keys(bot.channels).length,
					user_count: Object.keys(bot.users).length,
					messages_sent: rows[0].msgCnt
				}
			});
		});
	});
router.route('/user')
	.get(function(req, res) {
		db.clq({
			type: 'select',
			what: '*',
			location: 'users',
			id: 'userID',
			where: db.cache.apiusers[req.headers.key].userID
		}, function(err, rows) {
			if (err)
				res.send(err);
			delete rows[0].servers;
			let json = {
				err: false,
				data: rows[0]
			};
			res.json(json);
		});
	});
router.route('/user/:userID')
	.get(function(req, res) {
		if (db.cache.apiusers[req.headers.key].readStats) {
			db.clq({
				type: 'select',
				what: '*',
				location: 'users',
				id: 'userID',
				where: req.params.userID
			}, function(err, rows) {
				if (err)
					res.send(err);
				delete rows[0].servers;
				let json = {
					err: false,
					data: rows[0]
				};
				res.json(json);
			});
		} else {
			res.json({
				error: true,
				message: "You are not allowed to access this element"
			});
		}
	});
router.route('/user/:userID/timecounters')
	.get(function(req, res) {
		if (db.cache.apiusers[req.headers.key].readStats) {
			db.clq({
				type: 'select',
				what: '*',
				location: 'timecounters',
				id: 'userID',
				where: req.params.userID
			}, function(err, rows) {
				if (err)
					res.send(err);
				let json = {
					err: false,
					data: rows[0]
				};
				res.json(json);
			});
		} else {
			res.json({
				error: true,
				message: "You are not allowed to access this element"
			});
		}
	});
router.route('/channels/:channelID')
	.get(function(req, res) {
		if (db.cache.apiusers[req.headers.key].readStats) {
			db.clq({
				type: 'select',
				what: '*',
				location: 'channels',
				id: 'channelID',
				where: req.params.chansnelID
			}, function(err, rows) {
				if (err)
					res.send(err);
				let json = {
					err: false,
					data: rows[0]
				};
				res.json(json);
			});
		} else {
			res.json({
				error: true,
				message: "You are not allowed to access this element"
			});
		}
	});
router.route('/servers/:serverID')
	.get(function(req, res) {
		if (db.cache.apiusers[req.headers.key].readStats) {
			db.clq({
				type: 'select',
				what: '*',
				location: 'channels',
				id: 'channelID',
				where: req.params.channelID
			}, function(err, rows) {
				if (err)
					res.send(err);
				let json = {
					err: false,
					data: rows[0]
				};
				res.json(json);
			});
		} else {
			res.json({
				error: true,
				message: "You are not allowed to access this element"
			});
		}
	});

/* WEB ROUTES */

route.route('*')
	.all(function(req, res, next) {
		logger.info(chalk.gray("connection from: " + req.ip));
		/*if (req.useragent.source.match(/nikto|sqlmap|Jorgee/i)) {
			res.sendFile(`${__dirname}/10G.gzip`)
		}*/
		next();
	});
route.route('/')
	.get(function(req, res) {
		res.send($('html').html());
	});
route.route('/web/visit')
	.get(function(req, res) {
		logger.info(chalk.gray('Someone is currently visiting the bot documentation page'));
		db.clq({
			type: 'update',
			location: 'webstats',
			id: 'id',
			where: '1',
			change: [
				['visit'],
				[db.cache.webstats['1'].visit + 1]
			]
		});
		res.json({
			error: false,
			message: 'Successful'
		});
	});
route.route('/web/invite')
	.get(function(req, res) {
		logger.info(chalk.gray('Someone clicked the invite link'));
		db.clq({
			type: 'update',
			location: 'webstats',
			id: 'id',
			where: '1',
			change: [
				['invite'],
				[db.cache.webstats['1'].invite + 1]
			]
		});
		res.json({
			error: false,
			message: 'Successful'
		});
	});
route.route('/web/join')
	.get(function(req, res) {
		logger.info(chalk.gray('Someone clicked the join link'));
		db.clq({
			type: 'update',
			location: 'webstats',
			id: 'id',
			where: '1',
			change: [
				['`join`'],
				[db.cache.webstats['1'].join + 1]
			]
		});
		res.json({
			error: false,
			message: 'Successful'
		});
	});
route.route('/robots.txt')
	.get(function(req, res) {
		res.send($$('*').html());
	});


/* Starts the api Server */
app.use('/api', router);
app.use('/', route);

app.listen(port);