const fs = require('fs');
const chalk = require('chalk');
const Datastore = require('nedb');
const eventer = require('events');
const event = new eventer.EventEmitter();
const MainHold = new Datastore();
const ds = {};
const plugins = {};

let _loader = module.exports;
_loader.commands = {};
_loader.events = {};


module.exports = bot => {
	let commandFiles = fs.readdirSync(__dirname + '/commands');
	let eventFiles = fs.readdirSync(__dirname + '/events');
	let pluginFiles = fs.readdirSync(__dirname + '/plugins');
	pluginFiles.forEach((item) => {
		plugins[item.replace('.js', '')] = require(__dirname + `/plugins/${item}`);
	});
	const eventLister = file => {
		MainHold.insert({
			type: 'event',
			name: _loader.events[file].name,
			eventType: _loader.events[file].type,
			fileName: file
		});
	};

	eventFiles.forEach(file => {
		if (file.startsWith('index')) return;
		if (file.startsWith('help')) return;
		let fileName = file.replace(/\.js/, '');
		_loader.events[fileName] = require(`${__dirname}/events/${file}`)(bot, null, event);
		if (_loader.events[fileName].name)
			if (_loader.events[fileName].settings.autoLoad) {
				if (_loader.events[fileName].settings.datastore) datastore({
						input: fileName
					})
					.then(r => {
						delete _loader.events[fileName];
						_loader.events[fileName] = require(`${__dirname}/events/${file}`)(bot, r, event);
						eventLister(fileName);
					}).catch(e => console.log(e));
				else eventLister(fileName);
			} else delete _loader.events[fileName];
		else delete _loader.events[fileName];
		return _loader;
	});

	/**
	 * Command usage logging for the console, likely to be further expanded on in the future
	 * @param  {obj} module Object containinformation on the command module
	 * @param  {obj} meta   Main information container
	 * @param  {err} error  If theres a error it owuld be here
	 */
	const log = (module, meta, error, event) => {
		if (!event) {
			if (error) console.log(error);
			if (meta.error) {
				if (meta.messageSend) meta.messageSend("There was a error with that command");
				else return;
			}
			if (!module) module = {
				name: null
			};
			console.log(`info: ` + chalk.gray(chalk.cyan(module.name) + ` server: ${ meta.serverName } channel: ${ meta.channelID } by User: ${ meta.user }(${ meta.userID })`));
		}
	};


	/**
	 * Creates a nedb datastore for each module and returns it
	 * @param  {obj} meta Main informaiton container
	 * @return {promise}  Returns a promise with the datastore
	 */
	const datastore = meta => {
		return new Promise((fulfill, reject) => {
			try {
				if (meta.input) {
					ds[meta.input] = new Datastore({
						filename: `./warehouse/${meta.input}.json`
					});
					fulfill(ds[meta.input]);
				}
			} catch (e) {
				reject(e);
			}
		});
	};
	/**
	 * Checks the main command datastore if the command ran is one of the commands or sub commands within it
	 * @param  {obj} meta Main information container
	 * @return {promise}  Returns a promise containing the commands identifying information
	 */
	const dbCheck = meta => {
		return new Promise((fulfill, reject) => {
			try {
				MainHold.find({
					type: 'tempCommand',
					tempName: meta.command.toLowerCase()
				}, (e, r) => {
					if (r.length === 0) return;
					else fulfill(r);
				});
				MainHold.find({
					type: 'subCommand',
					name: meta.command.toLowerCase()
				}, (e, r) => {
					if (r.length === 0) return;
					else fulfill(r[0]);
				});
				MainHold.find({
					$or: [{
						type: 'command',
						name: meta.command.toLowerCase()
					}, {
						type: 'command',
						shortName: meta.command.toLowerCase()
					}]
				}, (e, r) => {
					if (e) reject(e);
					if (r.length === 0) reject('error: ' + chalk.gray(`No docs found: ${meta.command}`));
					else if (r.length > 1) reject("More than one doc found");
					else fulfill(r[0]);
				});
			} catch (e) {
				reject(e);
			}
		});
	};

	/**
	 * Parses temporary commands that are sent by the user
	 * @param  {obj} m    The module that contains the meta command
	 * @param  {obj} meta Main information container
	 */
	const tempCommand = (m, meta) => {
		if (m.length === 1) {
			if (m[0].oneUse) MainHold.remove({
				id: m[0].id,
				idGroup: m[0].idGroup
			}, {
				multi: true
			});
			if (m[0].enforceUID) {
				if (meta.userID === m[0].userID) {
					log({
						name: m.tempName
					}, meta);
					event.emit('tempCommand', m[0], meta);
				}
			} else if (!m[0].enforceUID) {
				log({
					name: m.tempName
				}, meta);
				event.emit('tempCommand', m[0], meta);
			}
		} else {
			m.forEach((i) => {
				if (m[0].oneUse) MainHold.remove({
					id: m[0].id,
					idGroup: m[0].idGroup
				}, {
					multi: true
				});
				if (i.enforceUID && meta.userID === i.userID) {
					log({
						name: m.tempName
					}, meta);
					event.emit('tempCommand', i, meta);
				} else if (!i.enforceUID) {
					log({
						name: m.tempName
					}, meta);
					event.emit('tempCommand', i, meta);
				}
			});
		}
	};

	/**
	 * Second stage for command processing, processes any command arguments provided
	 * @param  {obj} meta Main informaiton container
	 * @return {promise}  Returns a promise
	 */
	const secondStage = meta => {
		return new Promise((fulfill, reject) => {
			try {
				if (/ /.test(meta.input)) {
					let argTest = meta.input;
					meta.command = meta.input.substring(0, meta.input.indexOf(' '));
					dbCheck(meta)
						.then(r => {
							if (Array.isArray(r)) tempCommand(r, meta);
							else {
								meta.args = argTest.split(' ');
								meta.args.splice(meta.command.indexOf(meta.command), 1);
								if (r.type === 'subCommand') meta.subCommand = meta.command;
								meta.command = r.fileName;
								permissionTester(meta)
									.catch(e => meta.messageSend(meta.channelID, e));
							}
						})
						.catch(e => reject(e));
				} else {
					meta.command = meta.input;
					dbCheck(meta)
						.then(r => {
							if (Array.isArray(r)) tempCommand(r, meta);
							else {
								if (r.type === 'subCommand') meta.subCommand = meta.command;
								meta.command = r.fileName;
								meta.args = [];
								permissionTester(meta)
									.catch(e => meta.messageSend(meta.channelID, e));
							}
						})
						.catch(e => reject(e));
				}
			} catch (e) {
				reject(e);
			}
		});
	};

	/**
	 * Final stage of command processing, runs the command
	 * @param  {obj} meta Main information container
	 */
	const run = meta => {
		if (meta.serverData !== undefined) {
			meta.message = meta.message.replace(meta.serverData.prefix, '');
			if (meta.subCommand) _loader.commands[meta.command].subCommand[meta.subCommand].do(meta, event)
				.then(r => log(r, meta))
				.catch((e, r) => {
					log(r, meta, e);
					meta.messageSend("A error has occured while running that command");
				});
			else _loader.commands[meta.command].do(meta, event)
				.then(r => log(r, meta))
				.catch((e, r) => {
					log(r, meta, e);
					meta.messageSend("A error has occured while running that command");
				});
		} else meta.messageSend(meta.userID, 'Hold on, retry that command. I was just updated and needed a second to restart');
	};

	/**
	 * Tests a users permissions against a commands required permissions, if any
	 * @param  {obj} meta Main information container
	 * @return {promise}  Returns a promise when process is done
	 */
	const permissionTester = meta => {
		return new Promise((fulfill, reject) => {
			meta.input = _loader.commands[meta.command].requiredPerms;
			switch (meta.userID) {
				case meta.ownerID:
					run(meta);
					fulfill();
					break;
				case meta.serverOP:
					run(meta);
					fulfill();
					break;
				default:
					if (meta.input === null) {
						run(meta);
						fulfill();
						return;
					} else {
						meta.input.every(p => {
							if (meta.permissions[p]) {
								run(meta);
								fulfill();
								return false;
							} else reject("You dont have a required Permission.");
						});
					}
			}
		});
	};

	/**
	 * Inserts commands, as well as subcommands into the MainHold db with the commands identifying infromation so it can be run
	 * @param  {string} fileName The name of the file the command is located in
	 */
	const commandList = fileName => {
		if (_loader.commands[fileName].subCommand) {
			Object.keys(_loader.commands[fileName].subCommand).forEach((item, i, arr) => {
				MainHold.insert({
					type: 'subCommand',
					name: arr[i],
					fileName: fileName
				});
			});
		}
		if (_loader.commands[fileName].shortName) {
			MainHold.insert({
				type: 'command',
				name: _loader.commands[fileName].name,
				shortName: _loader.commands[fileName].shortName,
				fileName: fileName
			});
		} else {
			MainHold.insert({
				type: 'command',
				name: _loader.commands[fileName].name,
				fileName: fileName
			});
		}
	};

	/**
	 * Runs through the files in the commands folder and parses if they are in the correct format to be run by the auto loader
	 * @param  {string} file String containing a filename retrived using fs from the commands folder
	 */
	commandFiles.forEach(file => {
		if (file.startsWith('index')) return;
		let fileName = file.replace(/\.js/, '');
		_loader.commands[fileName] = require(`${__dirname}/commands/${file}`)(bot, null, event);
		if (_loader.commands[fileName].name)
			if (_loader.commands[fileName].settings.autoLoad) {
				if (_loader.commands[fileName].settings.datastore) datastore({
						input: fileName
					})
					.then(r => {
						delete _loader.commands[fileName];
						_loader.commands[fileName] = require(`${__dirname}/commands/${file}`)(bot, r, event);
						commandList(fileName);
					}).catch(e => console.log(e));
				else commandList(fileName);
			} else delete _loader.commands[fileName];
		else delete _loader.commands[fileName];
		return _loader;
	});

	/**
	 * Creates a demprorary command from the information recived from the tempCreate event, it stores the information in the Main Command Hold
	 * @param  {obj} info Containes the infromation from the tempCreate event
	 */
	const tempCreate = info => {
		let doc = {
			type: `tempCommand`,
			tempName: info.name,
			parent: info.parent,
			id: info.group ? null : info.id,
			idGroup: info.group ? info.id : null,
			userID: info.enforceUID ? info.userID : null,
			enforceUID: info.enforceUID ? true : false,
			oneUse: info.oneUse ? true : false,
			expires: info.expire ? info.expire : null
		};
		MainHold.insert(doc);
	};

	event.on('update', (module, meta) => log(module, meta));
	event.on('error', (module, meta, error) => log(module, meta, error));
	event.on('tempCreate', info => tempCreate(info));
	event.on('deregisterTemp', info => MainHold.remove({
		id: info.id ? info.id : null,
		idGroup: info.idGroup ? info.idGroup : null
	}, {
		multi: true
	}));

	/**
	 * Called by the bot itsself whenever it detects someone trying to run a command, initilizes the process of detecting any possible command
	 * @param  {obj} meta Main information container, contains information related to the user, server, channel, and message the user sent
	 */
	_loader.run = meta => {
		meta.input = meta.message;
		meta.delete = () => {
			return new Promise((fulfill, reject) => {
				bot.deleteMessage({
					channelID: meta.channelID,
					messageID: meta.messageID
				}, (e, r) => {
					if (e) reject(e);
					else fulfill(r);
				});
			});
		};
		meta.messageSend = (m, e) => {
			return new Promise((fulfill, reject) => {
				try {
					bot.sendMessage({
						to: meta.channelID,
						message: e ? null : m,
						embed: e ? e : null
					}, (e, r) => {
						if (e) {
							console.log(e);
							reject(e);
						} else {
							console.log(chalk.gray(`${chalk.white('info:')} ID: ${r.id} cID: ${meta.channelID} M: ${r.content.substring(0, 30)}`));
							r.delete = () => {
								return new Promise((fulfill, reject) => {
									bot.deleteMessage({
										channelID: meta.channelID,
										messageID: r.id
									}, (e, r) => {
										if (e) reject(e);
										else fulfill(r);
									});
								});
							};
							r.edit = (m, e) => {
								return new Promise((fulfill, reject) => {
									bot.editMessage({
										channelID: meta.channelID,
										messageID: r.id,
										message: m ? m : '',
										embed: e ? e : null
									}, (e, r) => {
										if (e) reject(e);
										else fulfill(r);
									});
								});
							};
							r.delayDelete = (time) => {
								return new Promise((fulfill, reject) => {
									setTimeout(() => {
										bot.deleteMessage({
											channelID: meta.channelID,
											messageID: r.id
										}, (e, r) => {
											if (e) reject(e);
											else fulfill(r);
										});
									}, time);
								});
							};
							fulfill(r);
						}
					});
				} catch (e) {
					reject(e);
				}
			});
		};
		secondStage(meta).catch(e => console.log(e));
	};

	_loader.event = (rawEvent, db) => {
		let d = rawEvent.d;
		MainHold.find({
			type: 'event',
			eventType: rawEvent.t
		}, (e, r) => r.forEach((item) => {
			_loader.events[item.fileName].execute(d, db).then(e => log(e, null, null, true)).catch((m, e) => log(m, null, e, true));
		}));
	};

	/**
	 * Develepment only command, used to read and change any commands datastorage
	 * @param  {string} db Database that is to be returned
	 * @return {promise}   Returns a promise with the datastore
	 */
	_loader.dbEx = db => {
		return new Promise((fulfill, reject) => {
			try {
				ds[db].loadDatabase(() => {
					fulfill(ds[db]);
				});
			} catch (e) {
				if (e) {
					reject(e);
				}
			}
		});
	};
	//console.log(_loader)
	_loader.plugins = plugins;
	module.exports = _loader;
	return _loader;
};