var moment = require('moment');
module.exports = function(ID) {
	const timestamp = new Date(parseInt(ID) / 4194304 + 1420070400000);
	const createdM = moment.utc(timestamp);
	const created = createdM.format("MMMM Do YYYY, hh:mm:ss a");
	return created;
};