const _event = {};

module.exports = (bot) => {
	_event.name = `example`;
	_event.settings = {
		datastore: false,
		dataAccess: null,
		autoLoad: true
	};
	_event.type = `PRESENCE_UPDATE`;
	_event.execute = (rawEvent) => {
		return new Promise((fulfill, reject) => {
			try {
				if (rawEvent.game !== null && rawEvent.guild_id === '148129145791053824') {
					bot.addToRole({
						serverID: '148129145791053824',
						userID: rawEvent.user.id,
						roleID: `317013318067814400`
					}, (e) => {
						if (e) console.log(e);
					});
				} else if (rawEvent.guild_id === '148129145791053824') {
					bot.removeFromRole({
						serverID: '148129145791053824',
						userID: rawEvent.user.id,
						roleID: `317013318067814400`
					}, (e) => {
						if (e) console.log(e);
					});
				}
				fulfill(_event);
			} catch (e) {
				reject(_event, e);
			}
		});
	};
	return _event;
};