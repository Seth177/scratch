const _event = {};

module.exports = (bot, db) => {
	_event.name = `messageEdit`;
	_event.settings = {
		datastore: false,
		dataAccess: null,
		autoLoad: true
	};
	_event.type = `MESSAGE_UPDATE`;
	_event.execute = (rawEvent, db) => {
		return new Promise((fulfill, reject) => {
			try {
				var message = rawEvent.content;
				db.clq({
					type: 'update',
					location: 'messages',
					id: 'messageID',
					where: rawEvent.id,
					change: [
						[
							'edited',
							'editedmsg'
						],
						[
							1,
							message
						]
					]
				});
				fulfill(_event);
			} catch (e) {
				reject(_event, e);
			}
		});
	};
	return _event;
};