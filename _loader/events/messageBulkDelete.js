const _event = {};
const async = require('async');

module.exports = (bot, db) => {
	_event.name = `example`;
	_event.settings = {
		datastore: false,
		dataAccess: null,
		autoLoad: true
	};
	_event.type = `MESSAGE_DELETE_BULK`;
	_event.execute = (rawEvent) => {
		return new Promise((fulfill, reject) => {
			try {
				var ids = rawEvent.ids;
				async.forEachOf(ids, function(value) {
					db.clq({
						type: 'update',
						location: 'messages',
						id: 'messageID',
						where: value,
						change: [
							[
								'deleted'
							],
							[
								1
							]
						]
					});
				});
				fulfill(_event);
			} catch (e) {
				reject(_event, e);
			}
		});
	};
	return _event;
};