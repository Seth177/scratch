const _event = {};

module.exports = (bot, db) => {
	_event.name = `example`;
	_event.settings = {
		datastore: false,
		dataAccess: null,
		autoLoad: true
	};
	_event.type = `MESSAGE_DELETE`;
	_event.execute = (event) => {
		return new Promise((fulfill, reject) => {
			try {
				fulfill(_event);
			} catch (e) {
				reject(_event, e);
			}
		});
	};
	return _event;
};