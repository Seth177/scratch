const _command = {},
	ytdl = require('ytdl-core'),
	YouTube = require('youtube-node'),
	youTube = new YouTube(),
	config = require('../../../config.json'),
	Discord = require('discord.js'),
	moment = require('moment'),
	shortid = require('shortid'),
	client = new Discord.Client();
youTube.addParam('type', 'video');
youTube.addParam('eventType', 'completed');

setTimeout(() => {
	client.login(config.token);
}, 5000);

const songPoss = {},
	streams = {},
	dispatch = {},
	audioSteam = {};

let Pembed, doc, i, queue, db, bot, event, ready = false,
	playing = false;

youTube.setKey(config.Google);

/**
 * Quick string prototype to make easy generation of a youtube search url
 * @param  {string} search      String to be replaced
 * @param  {string} replacement String to replace with
 * @return {string}             Returns string with replaced values
 */
String.prototype.replaceAll = function(search, replacement) {
	var target = this;
	return target.replace(new RegExp(search, 'g'), replacement);
};

client.on('ready', () => ready = true);

const youtubeReg = /http[s]?:\/\/(www\.)?((youtube\.com)|(youtu\.be))/ig;
const youtubeIDreg = /http[s]?:\/\/(www\.)?((youtube\.com)|(youtu\.be))\/(watch\?v=)?|(&)(.{1,255})|(\?)(.{1,255})/ig;
//const youtubePlaylistReg = /http[s]?:\/\/(www\.)?((youtube\.com)|(youtu\.be))\/(watch\?v=)?(.{1,11})?|(\?list=)/ig;
const linkReg = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/ig;

/**
 * Parses the information of the queue and sends a message with a embed displaying the current playing song and the current queue
 * @param  {obj} meta      Main information container
 * @param  {obj} videoInfo Information on the current playing song
 * @param  {obj} r         Current song queue
 */
const nowPlaying = (meta, videoInfo, r) => {
	let currentTime = moment.utc(dispatch[meta.serverID].time * 1000).format('HH:mm:ss');
	console.log(currentTime);
	Pembed = {
		title: `${videoInfo.title}`,
		url: videoInfo.url,
		description: `Video requested by: ${videoInfo.user}\nLength: ${r[0].length}\n${dispatch[meta.serverID]._volume * 100}% volume`,
		thumbnail: videoInfo.thumbnail
	};
	Pembed.color = 0xbb0000;
	if (r.length > 1) {
		Pembed.fields = [];
		Pembed.description += `\n_ _\n***Queue:***\n***-----------------***`;
		for (let i = 1; i < r.length; i++) {
			Pembed.fields.push({
				name: '_ _',
				value: `${r[i].songPos}: [${r[i].title}](${r[i].url})\nLength: ${r[i].length}\nRequested by: ${queue[i].user}`,
				inline: true
			});
		}
	}
	try {
		meta.messageSend(null, Pembed)
			.then(r => r.delayDelete(10000)
				.catch(e => console.log(e)))
			.catch(e => console.log(e));
	} catch (e) { /**/ }
};

const playerControls = {
	volume: meta => {
		return new Promise((fulfill, reject) => {
			try {
				meta.args = meta.input.split(' ');
				if (meta.userVoiceID !== meta.botVoiceID) reject('Not in same voice channel', _command.subCommand.skip);
				else {
					meta.delete();
					dispatch[meta.serverID].setVolume(meta.args[1] / 100);
					fulfill(_command.subCommand.volume);
					meta.messageSend(`Volume is now ${meta.args[1]}%`)
						.then(r => r.delayDelete(10000)
							.catch(e => reject(e)))
						.catch(e => reject(e));
				}
			} catch (e) {
				reject(e, _command.subCommand.volume);
			}
		});
	},
	skip: meta => {
		return new Promise((fulfill, reject) => {
			try {
				if (meta.userVoiceID !== meta.botVoiceID) reject('Not in same voice channel', _command.subCommand.skip);
				else {
					dispatch[meta.serverID].end("Skip");
					meta.delete()
						.catch(e => reject(e));
					fulfill(_command.subCommand.skip);
				}
			} catch (e) {
				reject(e, _command.subCommand.skip);
			}

		});
	},
	resume: meta => {
		return new Promise((fulfill, reject) => {
			try {
				if (meta.userVoiceID !== meta.botVoiceID) reject('Not in same voice channel', _command.subCommand.skip);
				else {
					dispatch[meta.serverID].resume();
					audioSteam[meta.serverID].resume();
					meta.messageSend("Resumeing the song")
						.then(r => r.delayDelete(5000)
							.catch(e => reject(e)))
						.catch(e => reject(e));
					meta.delete()
						.catch(e => reject(e));
					fulfill(_command.subCommand.resume);
				}
			} catch (e) {
				reject(e, _command.subCommand.resume);
			}
		});
	},
	pause: meta => {
		return new Promise((fulfill, reject) => {
			try {
				if (meta.userVoiceID !== meta.botVoiceID) reject('Not in same voice channel', _command.subCommand.skip);
				else {
					dispatch[meta.serverID].pause();
					audioSteam[meta.serverID].pause();
					meta.messageSend("Pausing the song")
						.then(r => r.delayDelete(5000)
							.catch(e => reject(e)))
						.catch(e => reject(e));
					meta.delete()
						.catch(e => reject(e));
					fulfill(_command.subCommand.pause);
				}
			} catch (e) {
				reject(e, _command.subCommand.pause);
			}
		});
	}
};

const createPlayerControls = (meta, deregister) => {
	if (!deregister) {
		event.emit('tempCreate', {
			name: 'volume',
			parent: 'play',
			id: meta.serverID,
			group: true,
			enforceUID: false,
			oneUse: false,
			expires: false
		});
		event.emit('tempCreate', {
			name: 'skip',
			parent: 'play',
			id: meta.serverID,
			group: true,
			enforceUID: false,
			oneUse: false,
			expires: false
		});
		event.emit('tempCreate', {
			name: 'resume',
			parent: 'play',
			id: meta.serverID,
			group: true,
			enforceUID: false,
			oneUse: false,
			expires: false
		});
		event.emit('tempCreate', {
			name: 'pause',
			parent: 'play',
			id: meta.serverID,
			group: true,
			enforceUID: false,
			oneUse: false,
			expires: false
		});
		event.on('tempCommand', (cmd, m) => {
			if (cmd.idGroup === meta.serverID) playerControls[cmd.tempName](m)
				.catch(e => console.log(e));
		});
	} else event.emit('deregisterTemp', {
		idGroup: meta.serverID
	});
};

/**
 * Primary function, plays the first song in the queue
 * Function will repeat infinitly until queue is empty
 * @param  {obj} meta    Main information container
 * @param  {function} fulfill Fulfill function to signify completion
 * @param  {function} reject  Reject function to signify any command error
 */
const play = meta => {
	sort(meta)
		.then(r => {
			if (r.length > 0) client.channels.get(r[0].voiceID).join()
				.then(connection => {
					audioSteam[meta.serverID] = ytdl(r[0].url, {
						filter: 'audioonly'
					});
					let volume = 0.5;
					if (dispatch[meta.serverID] !== undefined) volume = dispatch[meta.serverID]._volume;
					let videoInfo = r[0];
					streams[meta.serverID] = connection;
					db.update({
						_id: r[0]._id
					}, {
						$set: {
							playing: true
						}
					});
					dispatch[meta.serverID] = streams[meta.serverID].playStream(audioSteam[meta.serverID], {
						volume: volume
					});
					playing = true;
					nowPlaying(meta, videoInfo, r);
					createPlayerControls(meta);
					dispatch[meta.serverID].on('end', () => {
						try {
							songPoss[meta.serverID].songPos--;
						} catch (e) {
							songPoss[meta.serverID] = r.length - 1;
						}
						remove(meta, db);
						db.remove({
							_id: videoInfo._id
						});
						sort(meta, db)
							.then(r => {
								if (r.length > 0) {
									playing = false;
									play(meta);
									return;
								} else {
									if (playing) meta.messageSend("Queue empty, leaving voice").then(r => r.delayDelete(10000)
											.catch(e => console.log(e)))
										.catch(e => console.log(e));
									playing = false;
									createPlayerControls(meta, true);
									songPoss[meta.serverID].songPos = 0;
									streams[meta.serverID].disconnect();
									db.remove({
										type: 'server',
										serverID: meta.serverID
									});
									delete streams[meta.serverID];
								}
							})
							.catch(e => console.log(e));
					});
					dispatch[meta.serverID].on('error', e => {
						meta.error = true;
						event.emit('error', _command, meta, e);
					});
				})
				.catch(e => {
					meta.error = true;
					event.emit('error', _command, meta, e);
				});
			else db.remove({
				type: 'server',
				serverID: meta.serverID
			});
		})
		.catch(e => {
			meta.error = true;
			event.emit('error', _command, meta, e);
		});
};

/**
 * Gets the queue for a server then sorts the results
 * @param  {obj} meta Main information container
 * @return {promise}  Returns a promise
 */
const sort = meta => {
	return new Promise((fulfill, reject) => {
		try {
			db.find({
				serverID: meta.serverID,
				type: 'queue'
			}).sort({
				songPos: 1
			}).exec((e, r) => {
				queue = r;
				fulfill(r);
			});
		} catch (e) {
			reject(e);
		}
	});
};

/**
 * Searches youtube then sends a emessage with a embed
 * Then waits for further user confirmation on the song choice.
 * @param  {obj} meta Main information container
 * @return {promise}  Returns a promise for further action upon compleation
 */
const searchYoutube = meta => {
	return new Promise((fulfill, reject) => {
		try {
			let search = meta.args.join(' ');
			if (search.indexOf('|') !== '-1') search = search.split('|');
			else search = [search];
			youTube.search(search[0], 12, {
				type: 'video'
			}, (e, r) => {
				let SearchID = shortid.generate();
				r.items.forEach((item, i) => {
					event.emit('tempCreate', {
						name: String(i + 1),
						parent: 'play',
						id: SearchID,
						group: true,
						userID: meta.userID,
						enforceUID: true,
						oneUse: true,
						expires: false
					});
				});
				event.emit('tempCreate', {
					name: 'cancel',
					parent: 'play',
					id: SearchID,
					group: true,
					userID: meta.userID,
					enforceUID: true,
					oneUse: true,
					expires: false
				});
				let embed = {
					title: `Search Results`,
					description: `Do ${meta.serverData.prefix}<number> to select a option or ${meta.serverData.prefix}cancel to stop`,
					url: `https://www.youtube.com/results?search_query=${search[0].replaceAll(' ', '+')}`,
					fields: [],
					footer: {
						text: moment(Date.now()).format('MMMM Do YYYY, hh:mm:ss a'),
						icon_url: 'https://www.youtube.com/favicon.ico'
					},
					color: parseInt('E62117', 16)
				};
				r.items.forEach((item, index) => {
					embed.fields.push({
						name: `_ _`,
						value: `${index + 1}. [${item.snippet.title}](https://youtu.be/${item.id.videoId})`,
						inline: false
					});
				});
				meta.messageSend(null, embed)
					.then(res => {
						event.on('tempCommand', (cmd, meta) => {
							if (cmd.idGroup === SearchID) {
								if (cmd.name === 'cancel') {
									res.delete()
										.catch(e => reject(e));
									meta.delete()
										.catch(e => reject(e));
									event.removeListener('tempCommand', () => { /**/ });
									reject();
								} else {
									res.delete()
										.catch(e => reject(e));
									meta.delete()
										.catch(e => reject(e));
									event.removeListener('tempCommand', () => { /**/ });
									fulfill(r.items[Number(cmd.tempName) - 1]);
								}

							}
						});
					})
					.catch(e => {
						meta.error = true;
						event.emit('error', _command, meta, e);
					});
			});
		} catch (e) {
			reject(e);
		}
	});
};

/**
 * Adds a url to the current queue for the server to be played when its turn arrives
 * @param  {obj} meta    Main information container
 * @param  {obj} doc     Information on the song to be enqueued
 * @param  {function} fulfill Passing along the commands fulfill function
 * @param  {function} reject  Passing along the commands reject function
 */
const enqueue = (meta, doc) => {
	db.insert(doc, e => {
		if (!e) {
			sort(meta, db)
				.then(r => {
					try {
						let currentTime = moment.utc(dispatch[meta.serverID].time * 1000).format('HH:mm:ss');
						console.log(currentTime);
					} catch (e) { /**/ }
					db.find({
						type: 'server',
						serverID: meta.serverID
					}, (e, r) => {
						if (r.length === 0) db.insert({
							type: 'server',
							serverID: meta.serverID
						});
						else return;
					});
					if (r.length > 1) {
						Pembed = {
							title: `Video requested by ${meta.user} has been added to the queue`
						};
						Pembed.color = 0xbb0000;
						Pembed.fields = [];
						Pembed.description = `***Queue:***\n***-----------------***`;
						for (let i = 0; i < r.length; i++) {
							Pembed.fields.push({
								name: `_ _`,
								value: r[i].playing ? `${r[i].songPos}: [${r[i].title}](${r[i].url}) ***(Now Playing)***\n${r[i].length}\nRequested by: ${r[i].user}` : `${r[i].songPos}: [${r[i].title}](${r[i].url})\nLength: ${r[i].length}\nRequested by: ${r[i].user}`,
								inline: true
							});
						}
					}
					if (r.length > 0 && r.length < 2) play(meta);
					else if (!playing && i === 0) play(meta);
					else meta.messageSend(null, Pembed)
						.then(r => r.delayDelete(20000)
							.catch(e => console.log(e)))
						.catch(e => console.log(e));
				})

			.catch(e => {
				meta.error = true;
				event.emit('error', _command, meta, e);
			});
		} else if (e.errorType === 'uniqueViolated') {
			meta.messageSend(`That song is already in the queue`)
				.catch(e => console.log(e));
		}
	});
};

/**
 * Function for youtube videos, it gets information relevent to retrive information on the video.
 * If no link is provided in the command arguments then the function searches youtube for a relevant video from the arguments
 * @param  {obj} meta    Main information container
 * @param  {function} fulfill Passing along the fulfill function of the command along
 * @param  {function} reject  Passing along the reject function of the command along
 */
const youtubeVideo = meta => {
	if (!songPoss[meta.serverID]) songPoss[meta.serverID] = {
		songPos: 1
	};
	if (songPoss[meta.serverID].songPos <= 0) songPoss[meta.serverID].songPos = 1;
	if (meta.type === 'url') {
		meta.args[0] = `https://youtu.be/${meta.args[0].replace(youtubeIDreg, '')}`;
		youTube.getById(meta.args[0].replace(youtubeIDreg, ''), (e, r) => {
			try {
				doc = {
					type: 'queue',
					url: meta.args[0],
					title: r.items[0].snippet.title,
					thumbnail: r.items[0].snippet.thumbnails.default,
					playing: false,
					songPos: songPoss[meta.serverID].songPos > 0 ? songPoss[meta.serverID].songPos++ : 1,
					provider: 'youtube',
					length: 0,
					user: meta.user,
					voiceID: meta.userVoiceID,
					userID: meta.userID,
					serverID: meta.serverID,
					channelID: meta.channelID
				};
				ytdl.getInfo(meta.args[0], {
						filter: 'audioonly'
					})
					.then(info => {
						doc.length = moment.utc(info.length_seconds * 1000).format('HH:mm:ss');
						doc.lengthRaw = info.length_seconds;
						enqueue(meta, doc);
					});
			} catch (e) {
				meta.error = true;
				event.emit('error', _command, meta, e);
			}
		});
	} else {
		try {
			searchYoutube(meta)
				.then(r => {
					doc = {
						type: 'queue',
						url: `https://youtu.be/${r.id.videoId}`,
						title: r.snippet.title,
						thumbnail: r.snippet.thumbnails.default,
						playing: false,
						songPos: songPoss[meta.serverID].songPos > 0 ? songPoss[meta.serverID].songPos++ : 1,
						provider: 'youtube',
						user: meta.user,
						length: 0,
						voiceID: meta.userVoiceID,
						userID: meta.userID,
						serverID: meta.serverID,
						channelID: meta.channelID
					};
					ytdl.getInfo(`https://youtu.be/${r.id.videoId}`, {
							filter: 'audioonly'
						})
						.then(info => {
							doc.length = moment.utc(info.length_seconds * 1000).format('HH:mm:ss');
							doc.lengthRaw = info.length_seconds;
							enqueue(meta, doc);
						});
				})

			.catch(() => { /**/ });
		} catch (e) {
			meta.error = true;
			event.emit('error', _command, meta, e);
		}
	}
};

/**
 * Lowers the song posisiton number of everything in queue, called whenever somehting is removed from the queue
 * @param  {obj} meta Object containing all relevent information about where and who executed a command, as well as useful functions
 * @return {Promise} Returns a promise for when the prosses has been completed
 */
const remove = meta => {
	return new Promise((fulfill, reject) => {
		try {
			db.find({
				serverID: meta.serverID,
				type: 'queue'
			}, (e, r) => {
				r.forEach(r => {
					db.update({
						_id: r._id
					}, {
						$set: {
							songPos: r.songPos - 1 > 0 ? r.songPos - 1 : 1
						}
					});
					fulfill();
				});
			});
		} catch (e) {
			reject(e);
		}
	});
};

/**
 * Tests the array of arguments for any links and as well if any persent links is a supported service
 * @param  {array} arr Array of command arguments
 * @return {promise} Returns a promise when the process is completed
 */
const testLink = arr => {
	return new Promise((fulfill, reject) => {
		try {
			if (!linkReg.test(arr[0])) fulfill({
				link: false,
				arr: arr,
				match: arr[0].match(linkReg)
			});
			else if (arr[0].match(youtubeReg)) fulfill({
				link: true,
				youtube: true,
				arr: arr,
				match: arr[0].match(linkReg)
			});
			else fulfill({
				link: true,
				youtube: false,
				arr: arr,
				match: arr[0].match(linkReg)
			});
		} catch (e) {
			reject(e);
		}
	});
};

/**
 * Command module is initilized here
 * @param  {obj} b Object of the bot client that is running the command
 * @param  {obj} ds  Object of autosupplied database file provided by _loader
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */
module.exports = (b, ds, e) => {
	bot = b;
	event = e;
	_command.name = 'play';
	_command.shortName = 'skip';
	_command.description = `This is a command to make scratch play a song in voice chat`;
	_command.settings = {
		datastore: true,
		autoLoad: true,
		undocumented: true,
		banned: ['148129146420068352']
	};
	_command.requiredPerms = null;
	_command.play = play;
	_command.do = (meta, e) => {
		return new Promise((fulfill, reject) => {
			try {
				event = e;
				meta.delete();
				if (!meta.userVoiceID) {
					meta.messageSend("You are not in a voice channel")
						.then(r => r.delayDelete(5000)
							.catch(e => reject(e)))
						.catch(e => reject(e));
					return;
				} else if (_command.settings.banned.indexOf(meta.userVoiceID) === -1 && ready) {
					testLink(meta.args)

					.then(r => {
							if (!r.link) {
								meta.type = 'search';
								youtubeVideo(meta, fulfill, reject);
								fulfill(_command);
							} else if (r.youtube) {
								meta.type = 'url';
								youtubeVideo(meta, fulfill, reject);
								fulfill(_command);
							} else {
								meta.messageSend("That link isnt supported")
									.then(r => r.delayDelete(5000)
										.catch(e => reject(e)))
									.catch(e => reject(e));
							}
						})
						.catch(e => reject(e, _command));
				} else if (!ready) meta.messageSend("The music functions aren't ready yet due to a recent reboot, please wait about 5 seconds and try again")
					.then(r => r.delayDelete(6000)
						.catch(e => reject(e)))
					.catch(e => reject(e));
				else if (_command.settings.banned.indexOf(meta.userVoiceID) !== -1) meta.messageSend("I won't play music in that channel")
					.then(r => r.delayDelete(5000)
						.catch(e => reject(e)))
					.catch(e => reject(e));
				else meta.messageSend("Coulden't play music due to a error")
					.then(r => r.delayDelete(5000)
						.catch(e => reject(e)))
					.catch(e => reject(e));
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	_command.subCommand = {
		clear: {
			name: 'clear',
			description: 'Clears the current music queue',
			requiredPerms: null,
			do: meta => {
				return new Promise((fulfill, reject) => {
					try {
						meta.delete();
						db.remove({
							serverID: meta.serverID,
							type: 'queue'
						}, {
							multi: true
						}, (e, r) => {
							console.log(e, r);
							meta.messageSend(`Cleared ${r} items from the queue`)
								.then(r => r.delayDelete(5000)
									.catch(e => reject(e)))
								.catch(e => reject(e));
							fulfill(_command.subCommand.clear);
						});
					} catch (e) {
						reject(e, _command.subCommand.clear);
					}
				});
			}
		},
		queue: {
			name: 'queue',
			description: 'This is a command to make scratch display the current queue in voice chat',
			requiredPerms: null,
			do: meta => {
				return new Promise((fulfill, reject) => {
					try {
						meta.delete();
						sort(meta, ds)
							.then(r => {
								if (r.length === 0) {
									meta.messageSend('The Queue is empty')
										.then(r => r.delayDelete(10000)
											.catch(e => reject(e)))
										.catch(e => reject(e));
									return;
								}
								let Qembed = {
									title: `Current Queue`,
								};
								Qembed.color = 0xbb0000;
								Qembed.fields = [];
								Qembed.description = `***Queue:***\n***-----------------***`;
								for (let i = 0; i < r.length; i++) {
									Qembed.fields.push({
										name: `_ _`,
										value: r[i].playing ? `${r[i].songPos}: [${r[i].title}](${r[i].url}) ***(Now Playing)***\nLength: ${r[i].length}\nRequested by: ${r[i].user}` : `${r[i].songPos}: [${r[i].title}](${r[i].url})\nLength: ${r[i].length}\nRequested by: ${r[i].user}`,
										inline: true
									});
								}
								meta.messageSend(null, Qembed)
									.then(r => r.delayDelete(10000));
							})

						.catch(e => reject(e, _command.subCommand.queue));
						fulfill(_command.subCommand.queue);
					} catch (e) {
						reject(e, _command.subCommand.queue);
					}
				});
			}
		},
		join: {
			name: 'join',
			description: 'Makes scratch join your current voice channel',
			requiredPerms: null,
			do: meta => {
				return new Promise((fulfill, reject) => {
					try {
						if (bot.servers[meta.serverID].members['169253633710358529'].voice_channel_id) {
							client.channels.get(meta.userVoiceID).join();
							meta.messageSend('I have joined! (Though please stop using this command, it isn\'t necessary anymore)')
								.then(r => r.delayDelete(5000)
									.catch(e => reject(e)))
								.catch(e => reject(e));
						}
					} catch (e) {
						reject(e);
					}
				});
			}
		},
		leave: {
			name: 'leave',
			description: 'Makes scratch leave your current voice channel',
			requiredPerms: null,
			do: meta => {
				return new Promise((fulfill, reject) => {
					try {
						if (meta.userVoiceID !== meta.botVoiceID) reject('Not in same voice channel', _command.subCommand.skip);
						else {
							client.channels.get(meta.userVoiceID).leave();
							meta.messageSend('I have left!')
								.then(r => r.delayDelete(5000)
									.catch(e => reject(e)))
								.catch(e => reject(e));
						}
					} catch (e) {
						reject(e);
					}
				});
			}
		}
	};
	if (ds) {
		ds.loadDatabase();
		db = ds;
		db.ensureIndex({
			fieldName: 'url',
			unique: true
		});
		db.find({
			type: 'server'
		}, (e, r) => {
			if (r.length === 0) return;
			else if (r.length > 1) r.forEach((item) => {
				setTimeout(() => {
					play({
						serverID: item.serverID
					});
				}, 6000);

			});
			else setTimeout(() => {
				play({
					serverID: r[0].serverID
				});
			}, 7000);
		});
		/*db.remove({
			type: 'queue'
		}, {
			multi: true
		});*/
	}
	return _command;
};