const _command = {};
const request = require('request');
const config = require('../../../config.json');

/**
 * Command module is initilized here
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */

module.exports = () => {
	_command.name = 'shorten';
	_command.shortName = 's';
	_command.description = `This is used to shorten really long links (works in DM)`;
	_command.usage = `shorten <link>`;
	_command.settings = {
		datastore: false,
		autoLoad: true
	};
	_command.requiredPerms = null;
	_command.do = meta => {
		return new Promise((fulfill, reject) => {
			request(`https://api-ssl.bitly.com/v3/shorten?longUrl=${meta.args[0]}&access_token=${config.bitLy}`, (error, response, body) => {
				try {
					body = JSON.parse(body);
					if (body.status_txt === 'OK') {
						meta.messageSend(`<@${meta.userID}> Here is a short url: ${body.data.url}`).catch(e => reject(e));
					} else {
						reject({
							e: 'No url was returened by the api'
						});
					}
					fulfill(_command);
				} catch (e) {
					reject(e, _command);
				}
			});
		});
	};
	return _command;
};