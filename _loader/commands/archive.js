const _command = {};
const async = require('async');

/**
 * Command module is initilized here
 * @param  {obj} bot Object of the bot client that is running the command
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */
module.exports = bot => {
	_command.name = `archive`;
	_command.shortName = `arc`;
	_command.description = `This command allows you to archive the pinned messages of a channel`;
	_command.usage = `archive #channel | where #channel is the channel to archive the pins`;
	_command.settings = {
		datastore: false,
		autoLoad: true
	};
	_command.requiredPerms = ['TEXT_MANAGE_MESSAGES', 'GENERAL_ADMINISTRATOR'];
	/* Main command Logic */
	_command.do = meta => {
		return new Promise((fulfill, reject) => {
			try {
				bot.getPinnedMessages({
					channelID: meta.channelID,
					serverID: meta.serverID
				}, (e, r) => async.forEachOf(r, value => {
					if (e) console.log(e);
					var archiveQ = [];
					if (value.attachments.length > 0) {
						archiveQ.push(value.attachments[0].url);
						setTimeout(() => {
							meta.messageSend(null, {
								title: `Picture posted by: ***${value.author.username}***`,
								description: value.content,
								image: {
									url: value.attachments[0].url,
									height: value.attachments[0].height,
									width: value.attachments[0].width
								},
								color: 0x297F02
							}).catch(e => reject(e));
							bot.deletePinnedMessage({
								channelID: meta.channelID,
								messageID: value.id
							});
							archiveQ.splice(archiveQ.indexOf(value.attachments[0].url), 1);
						}, 1500 * archiveQ.length);
					} else {
						let expression = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/i;
						value.url = value.content.match(expression);
						value.content = value.content.replace(expression, '');
						archiveQ.push(value.content);
						setTimeout(() => {
							meta.messageSend(null, {
								title: `Picture posted by: ***${value.author.username}***`,
								description: value.content,
								image: {
									url: value.url,
									height: 1500,
									width: 1250
								},
								color: 0x297F02
							}).catch(e => reject(e));
							bot.deletePinnedMessage({
								channelID: meta.channelID,
								messageID: value.id
							});
							archiveQ.splice(archiveQ.indexOf(value.content), 1);
						}, 1500 * archiveQ.length);
					}
				}));
				fulfill(_command);
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};