const _command = {};

/**
 * Command module is initilized here
 * @param  {obj} bot Object of the bot client that is running the command
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */
module.exports = bot => {
	_command.name = 'gag';
	_command.shortName = null;
	_command.description = `This is a administrative command which disallows a user from using text communication in the channel its used in`;
	_command.usage = `gag @user | where @user is a mention of the user to be gagged`;
	_command.settings = {
		datastore: false,
		autoLoad: true
	};
	_command.requiredPerms = ['TEXT_MANAGE_MESSAGES', 'GENERAL_ADMINISTRATOR'];
	_command.do = meta => {
		return new Promise((fulfill, reject) => {
			try {
				meta.delete();
				meta.input = meta.mentions[0].id;
				try {
					if (meta.db.cache.users[meta.input].gag) {
						meta.db.clq({
							type: 'update',
							location: 'users',
							id: 'userID',
							where: meta.input,
							change: [
								['gag'],
								[0]
							]
						}, () => {
							bot.deleteChannelPermission({
								userID: meta.input,
								channelID: meta.channelID,
							}, e => {
								if (e) console.log(e);
							});
							meta.messageSend('Ok they are no longer gagged').catch(e => reject(e));
						});
						meta.db.cache.users[meta.input].gag = 0;

					} else {
						meta.db.clq({
							type: 'update',
							location: 'users',
							id: 'userID',
							where: meta.input,
							change: [
								['gag'],
								[1]
							]
						}, () => {
							bot.editChannelPermissions({
								userID: meta.input,
								channelID: meta.channelID,
								deny: [11]
							}, e => {
								if (e) console.log(e);
							});
							meta.messageSend('Ok that user is gagged').catch(e => reject(e));
						});
						meta.db.cache.users[meta.input].gag = 1;
					}
				} catch (e) {
					meta.db.clq({
						type: 'select',
						what: '*',
						location: 'users',
						id: 'userID',
						where: meta.input
					}, () => meta.messageSend('Ok they are not gaged, due to a cache error')).catch(e => reject(e));
				}
				fulfill(_command);
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};