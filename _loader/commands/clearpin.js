const _command = {};
const async = require('async');

/**
 * Command module is initilized here
 * @param  {obj} bot Object of the bot client that is running the command
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */
module.exports = bot => {
	_command.name = `clearpins`;
	_command.shortName = `cp`;
	_command.description = `This command allows you to clear the pinned messages of a channel`;
	_command.usage = `archive #channel | where #channel is the channel to archive the pins`;
	_command.settings = {
		datastore: false,
		autoLoad: true
	};
	_command.requiredPerms = ['TEXT_MANAGE_MESSAGES', 'GENERAL_ADMINISTRATOR'];
	_command.do = meta => {
		return new Promise((fulfill, reject) => {
			try {
				bot.getPinnedMessages({
					channelID: meta.channelID,
					serverID: meta.serverID
				}, (e, r) => async.forEachOf(r, value => {
					if (e) console.log(e);
					setTimeout(() => {
						bot.deletePinnedMessage({
							channelID: meta.channelID,
							messageID: value.id
						});
					}, 1500);
				}));
				fulfill(_command);
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};