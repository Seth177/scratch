const _command = {};

/**
 * Command module is initilized here
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */
module.exports = () => {
	_command.name = `permissions`;
	_command.shortName = `perm`;
	_command.description = `This is a command used mostly for testing, it lists all permissions you have`;
	_command.usage = `permissions`;
	_command.settings = {
		datastore: false,
		autoLoad: true
	};
	_command.requiredPerms = null;
	_command.do = meta => {
		return new Promise((fulfill, reject) => {
			try {
				meta.messageSend(`\`\`\`json\n${JSON.stringify(meta.permissions, null, '\t')}\`\`\``).catch(e => reject(e));
				fulfill(_command);
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};