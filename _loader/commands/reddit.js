const _command = {};
const request = require('request');
const Datastore = require('nedb');
const ds = new Datastore();

const find = (arr, meta, num, f) => {
	return new Promise((fulfill, reject) => {
		let number;
		if (num === undefined) {
			number = 0;
			num = 0;
		} else {
			number = num + 1;
			num++;
		}
		num = number;
		const result = arr[number];
		if (number !== arr.length)
			ds.find({
				url: result.data.url,
				serverID: meta.serverID
			}, (e, r) => {
				if (e) reject(e);
				if (r.length !== 0)
					if (f) find(arr, meta, num, f);
					else find(arr, meta, num, fulfill);
				else {
					ds.insert({
						serverID: meta.serverID,
						url: result.data.url
					});
					if (f) f(result);
					else fulfill(result);
				}
			});
		else reject();
	});
};

module.exports = () => {
	_command.name = `reddit`;
	_command.shortName = 'r';
	_command.description = `This is used to search reddit for a image/post`;
	_command.usage = `reddit <subreddit> | (note you dont need the r/)`;
	_command.settings = {
		datastore: false,
		autoLoad: true
	};
	_command.requiredPerms = null;
	_command.do = (meta) => {
		return new Promise((fulfill, reject) => {
			try {
				request('https://www.reddit.com/r/' + meta.args.join(' ') + '.json', function(error, response, body) {
					if (!error && response.statusCode == 200) {
						let redditJson = JSON.parse(body);
						let posts = redditJson.data.children;
						find(posts, meta).then(result => {
							let img = result.data.url;
							img = img.replace(/amp;/g, '');
							let embed = {
								title: result.data.title,
								url: img,
								image: {
									url: /\.jpg|\.png|\.gif/i.test(img) ? img : `${img}.jpg`
								}
							};
							if (result.data.over_18) {
								if (meta.channelData.nsfw) {
									meta.messageSend(null, embed).catch(e => reject(e));
								} else {
									meta.messageSend('That post is marked NSFW and this is a SFW channel!');
								}
							} else meta.messageSend(null, embed).catch(e => reject(e));
						}).catch((e) => {
							meta.messageSend("There was no non-duplicate images returned");
							console.log(e);
						}).then(r => r.delayDelete(5000).catch(e => reject(e))).catch(e => reject(e));
					}
				});
				fulfill(_command, meta);
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};