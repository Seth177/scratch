const _command = {};
const eBall = [
	"Are you kidding?",
	"As I see it, yes.",
	"Ask again later.",
	"Better not tell you now.",
	"Cannot predict now.",
	"Concentrate and ask again.",
	"Definitely not.",
	"Don't bet on it.",
	"Don't count on it.",
	"Forget about it.",
	"Go for it!",
	"I have my doubts.",
	"It is certain.",
	"It is decidedly so.",
	"Looking good!",
	"Looks good to me!",
	"Most likely.",
	"My reply is no.",
	"My sources say no.",
	"Outlook good.",
	"Outlook not so good.",
	"Outlook so so.",
	"Probably.",
	"Reply hazy, try again.",
	"Signs point to yes.",
	"Very doubtful.",
	"Who knows?",
	"Without a doubt.",
	"Yes - definitely.",
	"Yes, in due time.",
	"Yes.",
	"You may rely on it.",
	"You will have to wait."
];

/**
 * Command module is initilized here
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */
module.exports = () => {
	_command.name = '8ball';
	_command.shortName = `8`;
	_command.description = `Ask a magic 8 ball a question`;
	_command.usage = `test`;
	_command.settings = {
		datastore: false,
		autoLoad: true
	};
	_command.requiredPerms = null;
	_command.do = meta => {
		return new Promise((fulfill, reject) => {
			try {
				var resp = eBall[Math.floor(Math.random() * eBall.length)];
				meta.messageSend(`<@${meta.userID}> ${resp}`).catch(e => reject(e));
				fulfill(_command);
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};