const _command = {};
const where = require('node-where');
const request = require('request');
const moment = require('moment');
const config = require('../../../config.json');

/*
	To be honest, I dont know what this function does.
 */
var weather = {
	concatSummary: (input) => {
		let words = input.split(' ');
		let shortened;

		if (words[1] && (words[1].startsWith('(') || words[1].startsWith('in') || words[1].startsWith('starting') || words[1].startsWith('until') || words[1].startsWith('throughout'))) {
			shortened = words.splice(0, 1).join(' ');
		} else {
			shortened = words.splice(0, 2).join(' ');
		}

		return shortened;
	}
};
var toTitleCase = str => {
	return str.replace(/\w\S*/g, function(txt) {
		return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
	});
};

/**
 * Command module is initilized here
 * @param  {obj} bot Object of the bot client that is running the command
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */
module.exports = bot => {
	_command.name = 'weather';
	_command.shortName = `w`;
	_command.description = `This is to lookup the weather of a area`;
	_command.settings = {
		datastore: true,
		autoLoad: true
	};
	_command.requiredPerms = null;
	_command.do = meta => {
		return new Promise((fulfill, reject) => {
			try {
				if (meta.args.indexOf(/setlocation/im) !== -1) {
					meta.args.splice(meta.args.indexOf(/setlocation/im), 1);
					meta.messageSend('Setting location isn\'t fully implimented yet');
				} else {
					request(`http://www.mapquestapi.com/geocoding/v1/address?key=${config.mq}&location=%{meta.args.join('%20')}`, (error, response, body) => {
						if (result) {
							request(`https://api.darksky.net/forecast/b6970bb05f66b2160d5904054fdba800/${result.get('lat')},${result.get('lng')}`, (e, r, b) => {
								let response = JSON.parse(b);
								const momentNow = moment(new Date()).tz(response.timezone);
								let embedObj = {
									type: "rich",
									title: `Weather for ${toTitleCase(meta.args.join(' '))}`,
									description: `[Powered by Dark Sky](https://darksky.net/poweredby/)\n${response.hourly.summary} ${response.daily.summary} [Click here to see the map](https://maps.darksky.net/@temperature,${response.latitude},${response.longitude},5).`,
									color: 0x9c00cf,
									fields: [],
									footer: {
										text: `${momentNow.format('h:mma dddd, MM-DD-YYYY')} (${response.timezone})`,
										icon_url: 'http://cdn.fortiscoregaming.com/claptrap/darksky-icons/clock.png'
									},
									thumbnail: {
										url: `http://cdn.fortiscoregaming.com/claptrap/darksky-icons/${response.currently.icon}.png`,
									}
								};
								// alerts
								if (response.alerts && response.alerts.length > 0) {
									let alertLines = '';
									response.alerts.forEach((alert) => {
										// alertLines += `[${alert.title}](${alert.uri})`;
										alertLines += `\n${alert.title}`;
									});

									embedObj.fields.push({
										name: `IMPORTANT ALERTS`,
										value: alertLines,
										inline: false
									});
								}
								// current conditions
								let curTemp = `${Math.round(response.currently.temperature)}°F`;
								embedObj.fields.push({
									name: 'Current',
									value: `${curTemp}\n${(response.currently.humidity * 100).toFixed(0)}% Humidity\n${(response.currently.precipProbability * 100).toFixed(0)}% Precipitation\n${weather.concatSummary(response.currently.summary)}\n_ _`,
									inline: true
								});
								// forecast
								let daysAdded = 0;
								response.daily.data.forEach((day) => {
									if (daysAdded < 5) {
										daysAdded++;
										const momentDay = daysAdded === 1 ? momentNow : momentNow.add(1, 'day');
										const dayName = daysAdded === 1 ? `Today` : momentDay.format('dddd');
										let dayMin = `${Math.round(day.temperatureMin)}°F`;
										let dayMax = `${Math.round(day.temperatureMax)}°F`;
										embedObj.fields.push({
											name: `${dayName}`,
											value: `${dayMin} / ${dayMax}\n${(day.humidity * 100).toFixed(0)}% Humidity\n${(day.precipProbability * 100).toFixed(0)}% Precipitation\n${weather.concatSummary(day.summary)}\n _ _`,
											inline: true
										});
									}
								});
								// send the message
								meta.messageSend(null, embedObj).catch(e => reject(e));
							});
						}
					});
				}
				fulfill(_command);
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};