const math = require('mathjs');
const _command = {};

/**
 * Command module is initilized here
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */
module.exports = () => {
	_command.name = 'math';
	_command.shortName = 'm';
	_command.description = `This is a to do math for you`;
	_command.usage = `math {equation}`;
	_command.settings = {
		datastore: false,
		autoLoad: true
	};
	_command.requiredPerms = null;
	_command.do = meta => {
		return new Promise((fulfill, reject) => {
			try {
				var mathcmd = meta.message;
				var mathcall = mathcmd.replace('math ', '');
				try {
					meta.messageSend(`<@${ meta.userID }> the answer is this: ` + math.eval(mathcall)).catch(e => reject(e));
				} catch (e) {
					console.log(e);
					meta.messageSend("Sorry I'm unable to run that").catch(e => reject(e));
				}
				fulfill(_command);
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};