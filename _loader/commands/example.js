const _command = {};

module.exports = (bot, db) => {
	_command.name = `test`;
	_command.shortName = null;
	_command.description = `This is a dummy command to use as a example/placeholder`;
	_command.usage = `test`;
	_command.settings = {
		datastore: false,
		autoLoad: true
	};
	_command.requiredPerms = null;
	_command.do = (meta) => {
		return new Promise((fulfill, reject) => {
			try {
				fulfill(_command, meta);
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};