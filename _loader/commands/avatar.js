const _command = {};

/**
 * Command module is initilized here
 * @param  {obj} bot Object of the bot client that is running the command
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */
module.exports = bot => {
	_command.name = 'avatar';
	_command.shortName = `a`;
	_command.description = `This is a command used to post a larger version of the user, or another users avatar`;
	_command.usage = `avatar | avatar @user`;
	_command.settings = {
		datastore: false,
		autoLoad: true
	};
	_command.requiredPerms = null;
	_command.do = meta => {
		return new Promise((fulfill, reject) => {
			try {
				let ext;
				if (/^a_/.test(bot.users[meta.userID].avatar))
					ext = `.gif`;
				else
					ext = `.png`;
				meta.messageSend(null, {
					image: {
						url: `https://cdn.discordapp.com/avatars/${ meta.mentions[0] ? meta.mentions[0].id : meta.userID }/${ meta.mentions[0] ? meta.mentions[0].avatar :bot.users[meta.userID].avatar }${ ext }`,
						height: 128,
						width: 128
					},
					color: 0x6B81CA
				}).catch(e => reject(e));
				fulfill(_command);
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};