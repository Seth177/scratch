const _command = {};

/**
 * Command module is initilized here
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */
module.exports = () => {
	_command.name = 'musicmode';
	_command.shortName = null;
	_command.description = `This is a depricated command, likely will be removed unless I figure out something better with it`;
	_command.usage = `musicmode`;
	_command.settings = {
		datastore: false,
		autoLoad: false
	};
	_command.requiredPerms = ['GENERAL_MANAGE_GUILD', 'GENERAL_ADMINISTRATOR'];
	_command.do = meta => {
		return new Promise((fulfill, reject) => {
			try {
				if (meta.db.cache.channels[meta.channelID].music) {
					meta.db.clq({
						type: 'update',
						location: 'channels',
						id: 'channelID',
						where: meta.channelID,
						change: [
							['music'],
							[0]
						]
					});
					meta.db.cache.channels[meta.channelID].music = 0;
					meta.messageSend("This channel is now marked as not a music channel").catch(e => reject(e));
				} else {
					meta.db.clq({
						type: 'update',
						location: 'channels',
						id: 'channelID',
						where: meta.channelID,
						change: [
							['music'],
							[1]
						]
					});
					meta.db.cache.channels[meta.channelID].music = 1;
					meta.messageSend("This channel is now marked as a music channel! I will post information about songs linked in this channel if i can find it").catch(e => reject(e));
				}
				fulfill(_command);
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};