const _command = {};
const request = require('request');
const imgTimeout = {};

/**
 * Checks if the user has used the command multiple times in the past rapidly, and if so bans the user from further usage
 * @param  {obj} meta Main information container
 * @return {bool}     Returns a boolean weather or not the user is banned
 */
const timechecker = meta => {
	if (imgTimeout[meta.userID] === undefined) {
		imgTimeout[meta.userID] = {
			count: 1,
			timeout: Date.now() + 60000,
			banned: false
		};
		return true;
	} else if (!imgTimeout[meta.userID].banned) {
		if (imgTimeout[meta.userID].count > 20 && (Date.now() - imgTimeout[meta.userID].timeout < 60000)) {
			imgTimeout[meta.userID].banned = true;
			imgTimeout[meta.userID].timeout = Date.now() + 30000;
			return true;
		} else {
			if (Date.now() > imgTimeout[meta.userID].timeout) {
				imgTimeout[meta.userID].count = 1;
				imgTimeout[meta.userID].timeout = Date.now() + 10000;
				return true;
			} else {
				imgTimeout[meta.userID].count++;
				return true;
			}
		}
	} else {
		if (Date.now() > imgTimeout[meta.userID].timeout) {
			imgTimeout[meta.userID].banned = false;
			return true;
		} else {
			return false;
		}
	}
};

/**
 * LAzy function to change a string into title case
 * @param  {string} str String to turn into title case
 * @return {string}     Returns title cased string
 */
const toTitleCase = str => {
	return str.replace(/\w\S*/g, function(txt) {
		return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
	});
};

/**
 * Searches giphy for a relevant gif
 * @param  {object}   meta     Main information container
 * @param  {Function} callback Callback function
 * @return {null}              Returns if no results returned
 */
const giphy = (meta, callback) => {
	meta.input = meta.input.replace(' ', '+');
	let rating;
	if (meta.channelData.nsfw) rating = 'r';
	else rating = 'pg-13';
	request(`http://api.giphy.com/v1/gifs/search?q=${meta.input}}&api_key=dc6zaTOxFJmzC&rating=${rating}&sort=relevant`, function(error, response, body) {
		try {
			if (!error && response.statusCode == 200) {
				response = JSON.parse(body);
				if (response.data.length === 0) {
					callback({
						data: []
					}, null);
					return;
				}
				let data = response.data[Math.floor(Math.random() * (25 - 1 + 1)) + 1];
				meta.response = data;
				let imageEmbed = data.images.original;
				meta.response.images = {
					original: imageEmbed
				};
				callback(null, meta);
			} else {
				callback(error, null);
			}
		} catch (e) {
			console.log(e, response);
			callback(response, null);
		}
	});
};

/**
 * Command module is initilized here
 * @param  {obj} bot Object of the bot client that is running the command
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */
module.exports = bot => {
	_command.name = 'giphy';
	_command.shortName = null;
	_command.description = `This is a command to search for a relavent gif`;
	_command.usage = `giphy {search}`;
	_command.settings = {
		datastore: false,
		autoLoad: true
	};
	_command.requiredPerms = null;
	_command.do = meta => {
		return new Promise((fulfill, reject) => {
			try {
				if (timechecker(meta)) {
					meta.input = meta.message.replace('giphy ', '');
					if (meta.input.length <= 1) meta.input = 'random';
					giphy(meta, (e, r) => {
						if (e) {
							if (e.data.length === 0) meta.messageSend("No results were returned").then(r => r.delayDelete(1000)).catch(e => reject(e));
							else meta.messageSend("There was a error in processing that.").then(r => r.delayDelete(1000)).catch(e => reject(e));
							return;
						}
						r.input.replace(/!giphy |!gif /i, '');
						let embed = {
							title: `Results for: ${toTitleCase(r.input.replace('+', ' '))}`,
							url: r.response.url,
							fields: [{
								name: `Rating`,
								value: r.response.rating
							}, {
								name: `Post URL`,
								value: r.response.bitly_url
							}],
							image: r.response.images.original
						};
						if (r.response.source_tld.length > 1) embed.fields.push({
							name: `Source Website`,
							value: `http://${r.response.source_tld}`
						});
						if (r.response.user) {
							embed.fields.push({
								name: `User`,
								value: `${r.response.user.display_name}`
							});
							embed.thumbnail = {
								url: r.response.user.avatar_url,
								width: 50,
								height: 50
							};
						}
						meta.messageSend(null, embed).catch(e => reject(e));
					});
				}
				fulfill(_command);
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};