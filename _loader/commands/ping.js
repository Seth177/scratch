const _command = {};

/**
 * Command module is initilized here
 * @param  {obj} bot Object of the bot client that is running the command
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */
module.exports = () => {
	_command.name = 'ping';
	_command.shortName = null;
	_command.description = `This is a command to test connection`;
	_command.usage = `pong`;
	_command.settings = {
		datastore: false,
		autoLoad: true
	};
	_command.requiredPerms = null;
	_command.do = meta => {
		return new Promise((fulfill, reject) => {
			try {
				var start = Date.now();
				meta.delete();
				meta.messageSend('ping').then(res => {
					let time = Date.now() - start;
					res.edit(`pong - ${time} ms`).catch(e => console.log(e));
				}).catch(e => reject(e, _command));
				fulfill(_command);
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};