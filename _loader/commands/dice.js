const Roll = require('roll');
const _command = {};
const roll = new Roll();

/**
 * Command module is initilized here
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */
module.exports = () => {
	_command.name = 'roll';
	_command.shortName = 'r';
	_command.description = `This is a fun command, it allows you to roll some dice`;
	_command.usage = `roll [number]d[number]`;
	_command.settings = {
		datastore: false,
		autoLoad: true
	};
	_command.requiredPerms = null;
	_command.do = meta => {
		return new Promise((fulfill, reject) => {
			try {
				var dice = meta.args;
				if (dice[1].indexOf('d') === 0) {
					let dienum = roll.roll(dice[1]);
					meta.messageSend(`<@${meta.userID}> rolled: ${dienum.rolled.toString()}`).catch(e => reject(e));
				}
				//This is if theres more than one die thrown
				if (dice[1].indexOf('d') !== 0 && dice[1].indexOf('d') != -1) {
					var numdie = dice[1].substring(0, dice[1].toLowerCase().indexOf('d'));
					//This is to limit the number of die thrown
					if (numdie < 21) {
						let dienum = roll.roll(dice[1]);
						meta.messageSend(`<@${meta.userID}>rolled: ${dienum.rolled.toString()} For a total of: ${dienum.result}`).catch(e => reject(e));
					} else if (numdie > 21) {
						meta.messageSend(`<@${meta.userID}> Please roll no more than 20 dice`).catch(e => reject(e));
					}
				}
				//If now die are thrown toss this
				if (dice[1].indexOf('d') === -1) {
					meta.messageSend(`<@${meta.userID}> How can i roll a die with no dice to roll? :disappointed: (Note Accepted formats are numbers and die numbers such as d20 or 2d20`).catch(e => reject(e));
				}
				fulfill(_command);
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};