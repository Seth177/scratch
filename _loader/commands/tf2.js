const _command = {};

/**
 * Command module is initilized here
 * @param  {obj} bot Object of the bot client that is running the command
 * @param  {obj} db  Object of autosupplied database file provided by _loader
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */
module.exports = (bot, db) => {
	if (db) {
		db.loadDatabase();
		var doc = {
			_id: 'root',
		};
		db.insert(doc);
	}
	_command.name = 'temfortress2';
	_command.shortName = `tf2`;
	_command.description = `Tf2 News auto poster`;
	_command.usage = null;
	_command.settings = {
		datastore: true,
		autoLoad: false
	};
	_command.requiredPerms = null;
	_command.do = meta => {
		return new Promise((fulfill, reject) => {
			try {
				db.find({}, function(err, docs) {
					console.log(err, docs, meta);
				});
				fulfill(_command);
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};