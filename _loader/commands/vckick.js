const _command = {};
const moment = require('moment');

/**
 * Command module is initilized here
 * @param  {obj} bot Object of the bot client that is running the command
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */
module.exports = bot => {
	_command.name = `voicekick`;
	_command.shortName = `vckick`;
	_command.description = `This is a administrative command to kick a user from a voice channel`;
	_command.usage = `voicekick @user | where @user is a mention of the user to be kicked`;
	_command.settings = {
		datastore: false,
		autoLoad: true
	};
	_command.requiredPerms = [`GENERAL_KICK_MEMBERS`, `GENERAL_ADMINISTRATOR`];
	_command.do = meta => {
		return new Promise((fulfill, reject) => {
			try {
				let ext;
				meta.message = meta.message.replace(`vckick <@${meta.mentions[0].id}>`, '');
				bot.createChannel({
					serverID: meta.serverID,
					name: `to kick ${bot.users[meta.mentions[0].id].username} - ${meta.user}`,
					type: 'voice'
				}, (err, res) => {
					meta.inputID = res.id;
					bot.moveUserTo({
						serverID: meta.serverID,
						channelID: meta.inputID,
						userID: meta.mentions[0].id
					}, () => {
						bot.deleteChannel(meta.inputID);
						if (meta.serverID === '148129145791053824') {
							bot.removeFromRole({
								serverID: meta.serverID,
								userID: meta.mentions[0].id,
								roleID: '289553776539467776'
							});
						}
						if (/^a_/.test(bot.users[meta.mentions[0].id].avatar))
							ext = `.gif`;
						else
							ext = `.png`;
						let embed = {
							title: `${bot.users[meta.mentions[0].id].username} was just kicked from the voice channel by: ${meta.user}`,
							thumbnail: {
								url: `https://cdn.discordapp.com/avatars/${ meta.mentions[0].id }/${ bot.users[meta.mentions[0].id].avatar }${ext}`,
								width: 50,
								height: 50
							},
							footer: {
								text: moment(Date.now()).format(`MMMM Do YYYY, hh:mm:ss a`)
							},
							color: 0xD41B1B
						};
						meta.messageSend(null, embed).catch(e => reject(e));
					});
				});
				fulfill(_command);
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};