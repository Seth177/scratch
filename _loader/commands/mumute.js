const _command = {};
const async = require('async');

/**
 * Command module is initilized here
 * @param  {obj} bot Object of the bot client that is running the command
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */
module.exports = bot => {
	_command.name = `massunmute`;
	_command.shortName = `mumute`;
	_command.description = `This is a command to un-mute everyone in a voice channel of person who invoked it`;
	_command.usage = `massunmute`;
	_command.settings = {
		datastore: false,
		autoLoad: true
	};
	_command.requiredPerms = ['VOICE_MUTE_MEMBERS', 'GENERAL_ADMINISTRATOR'];
	_command.do = meta => {
		return new Promise((fulfill, reject) => {
			try {
				async.forEachOf(Object.keys(bot.servers[meta.serverID].channels[bot.servers[meta.serverID].members[meta.userID].voice_channel_id].members), value => {
					bot.unmute({
						serverID: meta.serverID,
						userID: value
					});
				});
				fulfill(_command);
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};