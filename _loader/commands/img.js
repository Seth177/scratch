const _command = {};
const googleimages = require('google-images');
const config = require('../../../config.json');
const Datastore = require('nedb');
const imagesearch = new googleimages(
	config.cse,
	config.Google
);
const imgTimeout = {};
let count = 0;
let imgNsfw = 'off';
let ds = new Datastore();

/**
 * Checks if the user has used the command multiple times in the past rapidly, and if so bans the user from further usage
 * @param  {obj} meta Main information container
 * @return {bool}     Returns a boolean weather or not the user is banned
 */
const timechecker = meta => {
	if (imgTimeout[meta.userID] === undefined) {
		imgTimeout[meta.userID] = {
			count: 1,
			timeout: Date.now() + 60000,
			banned: false
		};
		return true;
	} else if (!imgTimeout[meta.userID].banned) {
		if (imgTimeout[meta.userID].count > 20 && (Date.now() - imgTimeout[meta.userID].timeout < 60000)) {
			imgTimeout[meta.userID].banned = true;
			imgTimeout[meta.userID].timeout = Date.now() + 30000;
			return true;
		} else {
			if (Date.now() > imgTimeout[meta.userID].timeout) {
				imgTimeout[meta.userID].count = 1;
				imgTimeout[meta.userID].timeout = Date.now() + 10000;
				return true;
			} else {
				imgTimeout[meta.userID].count++;
				return true;
			}
		}
	} else {
		if (Date.now() > imgTimeout[meta.userID].timeout) {
			imgTimeout[meta.userID].banned = false;
			return true;
		} else {
			return false;
		}
	}
};

const find = (arr, meta, num, f) => {
	return new Promise((fulfill, reject) => {
		let number = Math.floor(Math.random() * arr.length);
		if (!num) num = [number];
		else num.push(number);
		while (num.indexOf(number) !== -1) number = number < arr.length - 1 ? number + 1 : 0;
		const result = arr[number];
		ds.count({
			url: result.data.url,
			serverID: meta.serverID
		}, (e, c) => {
			if (c !== arr.length)
				ds.find({
					url: result.data.url,
					serverID: meta.serverID
				}, (e, r) => {
					if (e) reject(e);
					if (r.length !== 0)
						if (f) find(arr, meta, num, f);
						else find(arr, meta, num, fulfill);
					else {
						ds.insert({
							serverID: meta.serverID,
							url: result.data.url
						});
						if (f) f(result);
						else fulfill(result);
					}
				});
			else reject();
		});
	});
};

/**
 * Command module is initilized here
 * @param  {obj} bot Object of the bot client that is running the command
 * @param  {obj} db  Object of autosupplied database file provided by _loader
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */
module.exports = bot => {
	/**
	 * Searches google for relevant images
	 * @param  {obj} meta Main information container
	 */
	const imageSearch = meta => {
		var bannedU = [];
		var imageColors = [0x2BD5F4, 0x1531D1, 0xA7D800, 0xA7D800, 0xA7D800, 0xCA00E0, 0xA142A0, 0x6B81CA];
		if (bannedU.indexOf(meta.userID) !== -1) {
			meta.messageSend('You have been disallowed from using this command');
		} else {
			if (/\blink\b|\bgerudo\b/ig.test(meta.input)) {
				meta.messageSend(null, {
					title: `Images Remaining: ${600-count}`,
					description: "Link",
					url: 'https://www.pinterest.com/pin/432838214164337123/',

					image: {
						url: 'https://s-media-cache-ak0.pinimg.com/originals/a1/3a/9b/a13a9be6d7cb1627789bc222cab1f693.jpg',
						height: 485,
						width: 396
					},
					color: imageColors[Math.floor(Math.random() * imageColors.length)],
					footer: {
						text: `${meta.userID}|${meta.messageID}`
					}
				}).then(r => {
					bot.addReaction({
						channelID: meta.channelID,
						messageID: r.id,
						reaction: '❌'
					});
				}).catch(e => console.log(e));
			} else {
				if (meta.channelData.nsfw) {
					imgNsfw = 'off';
				} else {
					imgNsfw = 'high';
				}
				imagesearch.search(meta.input, imgNsfw).then(function(images) {
					count++;
					if (images && images.length > 0) {
						find(images, meta).then(result =>
							meta.messageSend(null, {
								title: `Images Remaining: ${600-count}`,
								description: result.description,
								url: result.parentPage,
								fields: [{
									name: 'User',
									value: meta.user,
									inline: 1
								}, {
									name: 'Search',
									value: meta.input,
									inline: 1
								}],
								image: {
									url: result.url,
									height: result.height,
									width: result.width
								},
								color: imageColors[Math.floor(Math.random() * imageColors.length)],
								footer: {
									text: `${meta.userID}|${meta.messageID}`
								}
							}).then(r => {
								bot.addReaction({
									channelID: meta.channelID,
									messageID: r.id,
									reaction: '❌'
								});
							})).catch(e => console.log(e));
					} else {
						meta.sendMessage(`<@${meta.userID}>: I could not find a image for that.`).catch(e => console.log(e));
					}
				});
			}
		}
	};
	_command.name = 'image';
	_command.shortName = `img`;
	_command.description = `This is a command to search for a relavent image`;
	_command.usage = `img {search}`;
	_command.settings = {
		datastore: true,
		autoLoad: true
	};
	_command.requiredPerms = null;
	_command.do = meta => {
		return new Promise((fulfill, reject) => {
			try {
				if (timechecker(meta)) {
					var msg = meta.message;
					meta.input = msg.replace('img ', '');
					/*
						Array of nsfw words
					 */
					var words = ['boobs', 'boob', 'butts', 'anal', 'dick', 'cowgirl', 'penis', 'vagina', 'tits', 'titties', 'ass', 'vaginal', 'pussy', 'tit', 'scat', 'asshole', 'boob', 'butt', 'nipple', 'nipples', 'breasts', 'piss', 'sex', 'cum', 'jizz', 'cunt', 'cock', 'twink', 'vore', 'breast', 'guro', 'loli', 'lolicon', 'shota', 'shotacon', 'watersports', 'r34', 'rule34', 'porkymon', 'dildo', 'dildoes', 'fleshlight', 'onahole', 'nsfw', 'dicks', 'penises', 'tenga', 'boobie', 'boobies', 'semen', 'testicles', 'ballsack', 'futa', 'futanari', 'cuntboy', 'porn', 'sextoy', 'bluewaffle', 'lemonparty', 'mrhands', 'mr.hands', 'impregnation', 'cumdump', 'oral', 'blowjob', 'rimjob', 'titjob', 'boobjob', 'handjob', 'footjob', 'trap', 'buttjob', 'assjob', 'moonman', 'nsfw', 'necrophilia', 'pedophilia', 'erection', 'anus', 'yiff', 'scalie', 'genitals', 'genitalia', 'bestiality', 'anthrofied', 'masturbation', 'masturbating', 'bdsm', 's&m', 'ejaculation', 'clit', 'clitoris', 'labia', 'labium', 'vulva', 'cumshot', 'erect', 'cleavage', 'rape', 'pubes', 'pube', 'threesome', 'foursome', 'orgy', 'doggystyle', 'missionary', 'pokephilia', 'panties', 'upskirt', 'fingering', 'foreskin', 'flaccid', 'deflower', 'virgin', 'virginity', 'undressing', 'underwear', 'bra', 'incest', 'cunnilingus', 'lingerie', 'garter', 'garterbelt', 'penetrated', 'penetration', 'penetrate', 'teats', 'teat', 'lactation', 'lactating', 'herm', 'hermaphrodite', 'ballgag', 'fondle', 'fondling', 'fetish', 'cameltoe', 'mooseknuckle', 'intersex', 'dickgirl', 'bondage', 'gangbang', 'vibrator', 'knotting', 'urine', 'crap', 'shit', 'orgasm', 'ahegao', 'strapon', 'gore', 'nude', 'nudes', 'nudity', 'peeing', 'deepthroat', 'horny', 'ejaculate', 'analbeads', 'autofellatio', 'fellatio', 'diaperplay', 'cockring', 'circumcised', 'uncircumcised', 'mutilate', 'mutilation', 'mutilating', 'mutilated', 'buttplug', 'oviposition', 'multiboobs', 'multibreasts', 'multiballs', 'multiknot', 'multipussy', 'multivagina', 'multinipples', 'multitits', 'multititties', 'multidick', 'multicock', 'multipenis', 'hemipenes', 'diphallism', 'doujinshi', 'dojin', 'doujin', 'urethral', 'cakefarting', 'phallis', 'facesitting', 'masteryposition', 'muppetnecking', 'moobs', 'exhibitionism', 'hentai', 'bukkake', 'anvilposition', 'cumcovered', 'cumpool', 'cumpuddle', 'voyeurism', 'digiphilia', 'pasties', 'tribadism', 'scissoring', 'autoerotique', 'erotic', 'stripping', 'stripper', 'poledancer', 'poledancing', 'unbirthing', 'micropenis', 'petplay', 'tenting', 'trottla', 'realdoll', 'penispump', 'cockpump', 'dickpump', 'trainposition', 'ovum', 'pegging', 'selfcest', 'adultery', 'panty', 'sexmachine', 'fuckmachine', 'sexrobot', 'fuckrobot', 'sandwichposition', 'cp', 'goatse', 'pantyjob', 'hmv', 'shemale', 'milf', 'dilf', 'bbw', 'cuck', 'cuckold', 'fisting', 'lapdance', 'striptease', 'kkk', 'xxx', 'painal', 'whore', 'camwhore', 'camgirl', 'erotica', 'gloryhole', 'fap', 'lolita', 'lezbo', 'mmf', 'naked', 'ffm', 'sexo', 'wank', 'wanking', 'pornhub', 'xvideos', 'xhamster', 'e621', 'paheal', 'gelbooru', 'swfchan', 'iwara', 'nhentai', 'cherrypopping', 'beeg', 'madthumbs', 'pornerbros', 'tube8', 'redtube', 'youporn', 'eporner', 'youbunny', 'alphaporno', 'spankbank', 'spankwire', 'videolovesyou', 'whoreslag', 'bigtits', 'smalltits', 'shufuni', 'spankbang', 'matadormovies', 'pornwall', 'analpornhd', 'fapdu', 'userporn', 'fastjizz', 'xogogo', 'moviesand', 'free18', 'wetpussy', 'xxxbunker', 'keezmovies', 'vankoi', 'fucktube', 'youjizz', 'pornative', 'pornyep', 'dailee', 'pornsharia', 'sexfans', 'sluttyred', 'justfuckus', '24h-porn', 'extremetube', 'kuntfutube', 'youpunish', 'yobt', 'thisav', 'femdom', 'femdomtube', 'definefetish', 'nextdoordolls', 'eroxia', 'deviantclip', 'pornsitechoice', 'faptv', 'hardsextube', 'brazzers', 'brazzershdtube', 'morbid', 'booru', 'sankakucomplex', 'minus8', 'stoic5', 'darkmirage', 'scrungusbungus', 'manyakis', 'mikeinel', 'aogami', 'sicmop', 'jasonafex', 'animopron', 'shadbase', 'shadman', 'zonetan', 'showersex', 'cowgirlposition', 'reversecowgirlposition', 'tentacleporn', 'thighsex', 'thighjob', 'pussyjuice', 'yuri', 'Testicular holocaust', 'yaoi', 'zoophilia', 'lemme smash'];
					words = words.map(function(item) {
						return item.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
					});
					words = words.map(function(el) {
						return `\\b${el}\\b`;
					});
					var rx = RegExp(words.join("|"), 'i');
					if (count > 599) meta.messageSend("I'm sorry due to limitations im place by google I can only pull up 600 pictures, this goes across all servers I am in, and the other servers have used this count up. I'm sorry for the inconvenience, I will try to find a work around").catch(e => reject(e));
					else if (meta.input.match(rx)) {
						if (meta.channelData.nsfw) imageSearch(meta);
						else meta.messageSend("This channel is not marked as a NSFW channel").catch(e => reject(e));
					} else {
						let respectable = ['lenny', 'adolf', 'nazi', 'hitler', 'jew'];
						respectable = respectable.map(function(item) {
							return item.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
						});
						respectable = respectable.map(function(el) {
							return `\\b${el}\\b`;
						});
						rx = RegExp(respectable.join("|"), 'i');
						if (meta.input.match(rx)) meta.messageSend("One of those words where banned for different reasons, even though they arent nsfw.").catch(e => reject(e));
						else imageSearch(meta);
					}
				} else meta.messageSend('Slow Down!').catch(e => reject(e));
				fulfill(_command);
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};