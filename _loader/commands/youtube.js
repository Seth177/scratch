const _command = {};
const chalk = require('chalk');
const YouTube = require('youtube-node');
const youTube = new YouTube();
const config = require('../../../config.json');
const moment = require('moment');
youTube.setKey(config.Google);

/**
 * Command module is initilized here
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */
module.exports = () => {
	_command.name = 'youtube';
	_command.shortName = 'yt';
	_command.description = `This is used to search youtube for a video, channel, or playlist`;
	_command.usage = `yt <query>`;
	_command.settings = {
		datastore: false,
		autoLoad: true
	};
	_command.requiredPerms = null;
	_command.do = meta => {
		return new Promise((fulfill, reject) => {
			try {
				yt(meta).then(fulfill(_command)).catch(e => reject(e, _command));
			} catch (e) {
				reject(e, _command);
			}
		});
	};

	/**
	 * Really messy function to search youtube for a video, will likely be improved later taking some code from play.js's search function
	 * @param  {obj} meta Main information container
	 */
	function yt(meta) {
		return new Promise((fulfill, reject) => {
			try {
				youTube.search(meta.args.join(" "), 1, function(error, result) {

					if (error) {
						console.log(chalk.red(error));
					} else {
						let vtVideo = {
							title: result.items[0].snippet.title,
							desc: result.items[0].snippet.description,
							tags: result.items[0].snippet.tags
						};
						if (vtVideo.desc.length > 200) {
							vtVideo.desc = vtVideo.desc.substring(0, 200) + '...';
						}
						try {
							if (vtVideo.tags.length > 5) {
								vtVideo.tags.splice(5);
							}
						} catch (e) { /*.*/ }
						try {
							if (result.items[0].id.kind === 'youtube#video') {
								meta.messageSend('https://youtu.be/' + result.items[0].id.videoId).catch(e => reject(e));
								meta.messageSend(null, {
									title: vtVideo.title,
									description: '\n\n' + vtVideo.desc + '\n _ _',
									url: 'https://youtu.be/' + result.items[0].id.videoId,
									thumbnail: result.items[0].snippet.thumbnails.default,
									footer: {
										text: moment(Date.now()).format('MMMM Do YYYY, hh:mm:ss a'),
										icon_url: 'https://www.youtube.com/favicon.ico'
									},
									color: parseInt('E62117', 16)
								}).catch(e => reject(e));
								fulfill();
								meta.messageSend('https://youtu.be/' + result.items[0].id.videoId);
							} else if (result.items[0].id.kind === 'youtube#channel') {
								meta.messageSend(null, {
									title: vtVideo.title,
									description: '\n\n' + vtVideo.desc + '\n _ _',
									url: 'https://www.youtube.com/channel/' + result.items[0].id.channelId,
									thumbnail: result.items[0].snippet.thumbnails.default,
									footer: {
										text: moment(Date.now()).format('MMMM Do YYYY, hh:mm:ss a'),
										icon_url: 'https://www.youtube.com/favicon.ico'
									},
									color: parseInt('E62117', 16)
								}).catch(e => reject(e));
								fulfill();
							} else if (result.items[0].id.kind === 'youtube#playlist') {
								meta.messageSend(null, {
									title: vtVideo.title,
									description: '\n\n' + vtVideo.desc + '\n _ _',
									url: 'https://www.youtube.com/playlist?list=' + result.items[0].id.playlistId,
									thumbnail: result.items[0].snippet.thumbnails.default,
									footer: {
										text: moment(Date.now()).format('MMMM Do YYYY, hh:mm:ss a'),
										icon_url: 'https://www.youtube.com/favicon.ico'
									},
									color: parseInt('E62117', 16)
								}).catch(e => reject(e));
								fulfill();
							}
						} catch (e) {
							reject(e);
						}
					}
				});
			} catch (e) {
				reject(e, _command);
			}
		});
	}
	return _command;
};