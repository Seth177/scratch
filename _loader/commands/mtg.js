const _command = {};
const moment = require('moment');
const request = require('request');

/**
 * Command module is initilized here
 * @param  {obj} bot Object of the bot client that is running the command
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */
module.exports = bot => {
	_command.name = 'mtg';
	_command.shortName = null;
	_command.description = `This is a command to retrive information on a Magic the Gathering card`;
	_command.settings = {
		datastore: false,
		autoLoad: true
	};
	_command.requiredPerms = null;
	_command.do = meta => {
		return new Promise((fulfill, reject) => {
			try {
				request(`https://api.magicthegathering.io/v1/cards?name=${meta.args[0]}&pageSize=1`, (error, response, body) => {
					body = JSON.parse(body);
					meta.messageSend(null, {
						title: body.cards[0].name + ' Cost: ' + body.cards[0].manaCost,
						description: body.cards[0].text.toLowerCase(),
						url: 'http://gatherer.wizards.com/Pages/Card/Details.aspx?multiverseid=' + body.cards[0].multiverseid,

						fields: [{
							name: 'Set',
							value: body.cards[0].setName,
							inline: true
						}, {
							name: 'Type',
							value: body.cards[0].type,
							inline: true
						}, {
							name: 'Rarity',
							value: body.cards[0].rarity,
							inline: true
						}],
						footer: {
							text: moment(Date.now()).format('MMMM Do YYYY, hh:mm:ss a')
						},
						color: 0xE62117
					}).catch(e => reject(e));
				});
				fulfill(_command);
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};