const _command = {};

/**
 * Command module is initilized here
 * @param  {obj} bot Object of the bot client that is running the command
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */
module.exports = bot => {
	/**
	 * Primary function for the command, gets the specified ammount of messages up to a limit of 100 then maps them into a array and delets them
	 * @param  {obj} meta Main information container
	 */
	const messagesDelete = meta => {
		return new Promise((fulfill, reject) => {
			try {
				bot.getMessages({
					channelID: meta.channelID,
					limit: Number(meta.input),
					before: meta.messageID
				}, (error, messageArr) => {
					bot.deleteMessages({
						channelID: meta.channelID,
						messageIDs: messageArr.map(m => m.id)
					}, (e) => {
						if (e) reject(e);
						else fulfill();
					});
				});
			} catch (e) {
				reject(e);
			}
		});
	};

	_command.name = 'prune';
	_command.shortName = null;
	_command.description = `This is a command to mass delete messages`;
	_command.usage = `prune <number>`;
	_command.settings = {
		datastore: false,
		autoLoad: true
	};
	_command.requiredPerms = ['TEXT_MANAGE_MESSAGES', 'GENERAL_ADMINISTRATOR'];
	_command.do = meta => {
		return new Promise((fulfill, reject) => {
			try {
				if (typeof Number(meta.args[0]) !== 'number') reject({
					e: 'No number provided'
				}, _command);
				meta.delete();
				meta.input = Number(meta.args[0]);
				messagesDelete(meta)
					.then(setTimeout(function() {
						meta.messageSend(`Ok removed the last ${meta.args[0]} Messages`)
							.then(r => r.delayDelete(5000)
								.catch(e => reject(e)))
							.catch(e => reject(e));
					}, 500))
					.catch(() => meta.messageSend('There was a error removeing the messages, the likely cause is there was a message older than two weeks, cant mass delete that, sorry discord limitation')).catch(e => reject(e));

				fulfill(_command);
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};