const _command = {};

/**
 * Command module is initilized here
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */
module.exports = () => {
	_command.name = 'ignore';
	_command.shortName = null;
	_command.description = `This is acommand to make scratch ignore a channel`;
	_command.settings = {
		datastore: false,
		autoLoad: true
	};
	_command.requiredPerms = ['GENERAL_MANAGE_CHANNELS', 'GENERAL_ADMINISTRATOR'];
	_command.do = meta => {
		return new Promise((fulfill, reject) => {
			try {
				if (meta.channelData.ignore) {
					meta.db.clq({
						type: 'update',
						location: 'channels',
						id: 'channelID',
						where: meta.channelID,
						change: [
							['`ignore`'],
							[0]
						]
					}, () => {
						meta.messageSend('Ok no longer ignoring this channel').catch(e => reject(e));
					});
					meta.db.cache.channels[meta.channelID].ignore = 0;

				} else {
					meta.db.clq({
						type: 'update',
						location: 'channels',
						id: 'channelID',
						where: meta.channelID,
						change: [
							['`ignore`'],
							[1]
						]
					}, () => {
						meta.messageSend('Ok ignoring this channel').catch(e => reject(e));
					});
					meta.db.cache.channels[meta.channelID].ignore = 1;
				}
				fulfill(_command);
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};