const _command = {};

/**
 * Command module is initilized here
 * @param  {obj} bot Object of the bot client that is running the command
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */
module.exports = bot => {
	_command.name = 'unflipme';
	_command.shortName = `uflip`;
	_command.description = `This is a stupid command that fixes your name`;
	_command.usage = `flipme`;
	_command.settings = {
		datastore: false,
		autoLoad: true
	};
	_command.requiredPerms = null;
	_command.do = meta => {
		return new Promise((fulfill, reject) => {
			try {
				bot.editNickname({
					serverID: meta.serverID,
					userID: meta.userID,
					nick: null
				}, e => {
					if (e)
						meta.messageSend(`Sorry I couldent un-flip you`).catch(e => reject(e));
					else
						meta.messageSend(`I un-flipped you! You are now named ${ meta.user }`).catch(e => reject(e));
				});
				fulfill(_command);
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};