const _command = {};
const request = require('request');
const xkcd = require('xkcd-imgs');

/**
 * Main module initilization
 * @return {obj} Returns command metadata as well as the execution function
 */
module.exports = () => {

	/**
	 * Searches a api for a relevent xkcd pisture
	 * @param  {obj} meta Main information container
	 * @return {promise}  Returns a promise
	 */
	const relxkcd = meta => {
		return new Promise((fulfill, reject) => {
			try {
				request(`https://relevantxkcd.appspot.com/process?action=xkcd&query=${meta.args.join(' ')}`, (e, r, body) => {
					if (!e) {
						let comicnum = body.match(/^(\d){1,5}[^.\n ]/m)[0];
						request('http://xkcd.com/' + comicnum + '/info.0.json', (e, r, body) => {
							if (!e) {
								let xkcdJson = JSON.parse(body);
								fulfill(`${xkcdJson.title}\n\`\`\`${xkcdJson.alt}\`\`\`\n${xkcdJson.img}`);
							} else(reject(e, _command));
						});
					} else(reject(e, _command));
				});
			} catch (e) {
				reject(e);
			}
		});
	};
	_command.name = `xkcd`;
	_command.shortName = null;
	_command.description = `This is used to get a image from xkcd, or search for a relevant xkcd image`;
	_command.usage = `test`;
	_command.settings = {
		datastore: false,
		autoLoad: true
	};
	_command.requiredPerms = null;
	_command.do = (meta) => {
		return new Promise((fulfill, reject) => {
			try {
				if (meta.args.length === 0) xkcd.img((err, res) => {
					if (err) reject(err, _command);
					else meta.messageSend(`${res.title}\n${res.url}`).catch(e => reject(e));
				});
				else if (isNaN(meta.args.join(' '))) relxkcd(meta)
					.then(r => {
						meta.messageSend(r).catch(e => reject(e));
						fulfill(_command, meta);
					})
					.catch(e => reject(e, _command));
				else {
					request('http://xkcd.com/' + meta.args.join(' ') + '/info.0.json', (e, r, body) => {
						if (!e) {
							let xkcdJson = JSON.parse(body);
							meta.messageSend(`${xkcdJson.title}\n\`\`\`${xkcdJson.alt}\`\`\`\n${xkcdJson.img}`).catch(e => reject(e));
							fulfill(_command);
						} else {
							console.log(e);
							reject(e, _command);
						}
					});
				}

			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};