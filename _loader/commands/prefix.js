const _command = {};

/**
 * Command module is initilized here
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */
module.exports = () => {
	_command.name = 'prefix';
	_command.shortName = null;
	_command.description = `This is to change the prefix used for a server to execute commands`;
	_command.usage = `prefix <prefix>`;
	_command.settings = {
		datastore: false,
		autoLoad: true
	};
	_command.requiredPerms = ['GENERAL_MANAGE_GUILD', 'GENERAL_ADMINISTRATOR'];
	_command.do = meta => {
		return new Promise((fulfill, reject) => {
			try {
				if (meta.args[0])
					meta.db.clq({
						type: 'update',
						location: 'servers',
						id: 'serverid',
						where: meta.serverID,
						change: [
							['prefix'],
							[
								meta.args[0]
							]
						]
					}, () => {
						try {
							meta.db.cache.servers[meta.serverID].prefix = meta.args[0];
						} catch (e) { /**/ }
						meta.messageSend("The prefix for this server is now: " + meta.args[0]);
					});
				else meta.messageSend(`You need to provide a prefix in order to change it, the current prefix is ${meta.serverData.prefix}`);
				fulfill(_command);
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};