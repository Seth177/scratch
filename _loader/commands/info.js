const _command = {};
const doc = require('../../assets/doc.json');

/**
 * Command module is initilized here
 * @param  {obj} bot Object of the bot client that is running the command
 * @return {obj} Returns information pertaining to the command, as well as the execution function, and any subcommands connected to the command
 */
module.exports = bot => {
	_command.name = 'info';
	_command.shortName = null;
	_command.description = `This is a info me or about my current stats`;
	_command.usage = `info`;
	_command.settings = {
		datastore: false,
		autoLoad: true
	};
	_command.requiredPerms = null;
	_command.do = meta => {
		return new Promise((fulfill, reject) => {
			try {
				if (meta.args.indexOf('stats') !== -1) {
					var serverCnt = Object.keys(bot.servers).length,
						channelCnt = Object.keys(bot.channels).length,
						userCnt = Object.keys(bot.users).length;
					meta.messageSend(null, {
						title: `These are my current statistics`,
						url: `https://discordapp.com/channels/${ meta.serverID }/${ meta.channelID }`,
						description: "These are the current ammount of servers, channels, and users I`m connected to",
						fields: [{
							name: `Servers`,
							value: `Active: ${ serverCnt }`,
							inline: true
						}, {
							name: `Channels`,
							value: `Active: ${ channelCnt }`,
							inline: true
						}, {
							name: `Users`,
							value: `Active: ${ userCnt }`,
							inline: true
						}],
						color: parseInt(`A142A0`, 16)
					}).catch(e => reject(e));
				} else {
					meta.messageSend(null, {
						title: `Click here to join my help server!`,
						url: `https://discord.ratchtnet.com/invite`,
						description: doc.info + ` \n\n If you want to see my current statistics do ${ meta.serverData.prefix }info stats\nTo see a list of commands do ${ meta.serverData.prefix }help\nTo get help on a specific command do ${ meta.serverData.prefix }help <command>`,
						color: 0xA142A0
					}).catch(e => reject(e));
				}
				fulfill(_command);
			} catch (e) {
				reject(e, _command);
			}
		});
	};
	return _command;
};